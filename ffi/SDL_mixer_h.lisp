;;;;; SDL_mixer bindings for Corman Lisp
;;;;; Version 0.1, 2004/06/04
;;;;; Author: Luke Crook ( luke@balooga.com )
;;;;; SDL Version 1.2.7.0
;;;;; SDL Mixer version 1.2.5
;;;;; Corman Lisp Version 2.5
;;;;; 
;;;;;	TODO:
;;;;;       #define Mix_SetError	SDL_SetError

(require :windef)
(require :sdl)
(in-package :sdl)

;(in-package :win)

;;;;;       SDL_mixer.h,v 1.24 2002/05/21 05:45:59 slouken

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC" )
#define MIX_MAJOR_VERSION	1
#define MIX_MINOR_VERSION	2
#define MIX_PATCHLEVEL		5
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
/*
#define MIX_VERSION(X)							\
{									\
	(X)->major = MIX_MAJOR_VERSION;					\
	(X)->minor = MIX_MINOR_VERSION;					\
	(X)->patch = MIX_PATCHLEVEL;					\
}
*/
#LISP (progn
	(export 'MIX_VERSION)
	(defun MIX_VERSION (x)
        (setf (ct:cref SDL_version x major) MIX_MAJOR_VERSION)
        (setf (ct:cref SDL_version x minor) MIX_MINOR_VERSION)
        (setf (ct:cref SDL_version x patch) MIX_PATCHLEVEL)))

!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
extern DECLSPEC const SDL_version *  Mix_Linked_Version();
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
#define MIX_CHANNELS	8
#define MIX_DEFAULT_FREQUENCY	22050
//#define MIX_DEFAULT_FORMAT	AUDIO_S16LSB
#define MIX_DEFAULT_FORMAT	AUDIO_S16MSB
#define MIX_DEFAULT_CHANNELS	2
#define MIX_MAX_VOLUME		128	/* Volume of a chunk */
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC" :pack 4 )
typedef struct {
	int allocated;
	Uint8 *abuf;
	Uint32 alen;
	Uint8 volume;		/* Per-sample volume, 0-128 */
} Mix_Chunk;

typedef enum {
	MIX_NO_FADING,
	MIX_FADING_OUT,
	MIX_FADING_IN
} Mix_Fading;

typedef enum {
	MUS_NONE,
	MUS_CMD,
	MUS_WAV,
	MUS_MOD,
	MUS_MID,
	MUS_OGG,
	MUS_MP3
} Mix_MusicType;

typedef struct _Mix_Music Mix_Music;
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
extern DECLSPEC int  Mix_OpenAudio(int frequency, Uint16 format, int channels,
							int chunksize);
extern DECLSPEC int  Mix_AllocateChannels(int numchans);
extern DECLSPEC int  Mix_QuerySpec(int *frequency,Uint16 *format,int *channels);

extern DECLSPEC Mix_Chunk *  Mix_LoadWAV_RW(SDL_RWops *src, int freesrc);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
//#define Mix_LoadWAV(file)	Mix_LoadWAV_RW(SDL_RWFromFile(file, "rb"), 1)
#LISP (progn
	(export 'Mix_LoadWAV)
	(defun Mix_LoadWAV (file)
        (Mix_LoadWAV_RW 
            (SDL_RWFromFile file "rb") 1)))
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
extern DECLSPEC Mix_Music *  Mix_LoadMUS(const char *file);
//extern no_parse_DECLSPEC Mix_Music *  Mix_LoadMUS_RW(SDL_RWops *rw);
extern DECLSPEC Mix_Chunk *  Mix_QuickLoad_WAV(Uint8 *mem);
extern DECLSPEC Mix_Chunk *  Mix_QuickLoad_RAW(Uint8 *mem, Uint32 len);
extern DECLSPEC void  Mix_FreeChunk(Mix_Chunk *chunk);
extern DECLSPEC void  Mix_FreeMusic(Mix_Music *music);
extern DECLSPEC Mix_MusicType  Mix_GetMusicType(const Mix_Music *music);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
//extern DECLSPEC void SDLCALL Mix_SetPostMix(void (*mix_func)
//                             (void *udata, Uint8 *stream, int len), void *arg);
typedef void (*mix_func)(void *udata, Uint8 *stream, int len);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
extern DECLSPEC void  Mix_SetPostMix(mix_func m_f, void *arg);
//extern DECLSPEC void  Mix_HookMusic(void (*mix_func)
//                          (void *udata, Uint8 *stream, int len), void *arg);
extern DECLSPEC void  Mix_HookMusic(mix_func m_f, void *arg);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
//extern DECLSPEC void  Mix_HookMusicFinished(void (*music_finished)(void));
typedef void (*music_finished)(void);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
extern DECLSPEC void  Mix_HookMusicFinished(music_finished m_f);
extern DECLSPEC void *  Mix_GetMusicHookData();
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
//extern DECLSPEC void  Mix_ChannelFinished(void (*channel_finished)(int channel));
typedef void (*channel_finished)(int channel);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
extern DECLSPEC void  Mix_ChannelFinished(channel_finished c_f);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
#define MIX_CHANNEL_POST  -2

typedef void (*Mix_EffectFunc_t)(int chan, void *stream, int len, void *udata);
typedef void (*Mix_EffectDone_t)(int chan, void *udata);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
extern DECLSPEC int  Mix_RegisterEffect(int chan, Mix_EffectFunc_t f,
					Mix_EffectDone_t d, void *arg);
extern DECLSPEC int  Mix_UnregisterEffect(int channel, Mix_EffectFunc_t f);
extern DECLSPEC int  Mix_UnregisterAllEffects(int channel);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
#define MIX_EFFECTSMAXSPEED  "MIX_EFFECTSMAXSPEED"
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
extern DECLSPEC int  Mix_SetPanning(int channel, Uint8 left, Uint8 right);
extern DECLSPEC int  Mix_SetPosition(int channel, Sint16 angle, Uint8 distance);
extern DECLSPEC int  Mix_SetDistance(int channel, Uint8 distance);
//extern no_parse_DECLSPEC int  Mix_SetReverb(int channel, Uint8 echo);
extern DECLSPEC int  Mix_SetReverseStereo(int channel, int flip);
extern DECLSPEC int  Mix_ReserveChannels(int num);
extern DECLSPEC int  Mix_GroupChannel(int which, int tag);
extern DECLSPEC int  Mix_GroupChannels(int from, int to, int tag);
extern DECLSPEC int  Mix_GroupAvailable(int tag);
extern DECLSPEC int  Mix_GroupCount(int tag);
extern DECLSPEC int  Mix_GroupOldest(int tag);
extern DECLSPEC int  Mix_GroupNewer(int tag);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
extern DECLSPEC int  Mix_PlayChannelTimed(int channel, Mix_Chunk *chunk, int loops, int ticks);
extern DECLSPEC int  Mix_PlayMusic(Mix_Music *music, int loops);
extern DECLSPEC int  Mix_FadeInMusic(Mix_Music *music, int loops, int ms);
extern DECLSPEC int  Mix_FadeInMusicPos(Mix_Music *music, int loops, int ms, double position);
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
//#define Mix_PlayChannel(channel,chunk,loops) Mix_PlayChannelTimed(channel,chunk,loops,-1)
#LISP (progn
	(export 'Mix_PlayChannel)
	(defun Mix_PlayChannel (channel chunk loops)
        (Mix_PlayChannelTimed channel chunk loops -1)))
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
extern DECLSPEC int  Mix_FadeInChannelTimed(int channel, Mix_Chunk *chunk, int loops, int ms, int ticks);
extern DECLSPEC int  Mix_Volume(int channel, int volume);
extern DECLSPEC int  Mix_VolumeChunk(Mix_Chunk *chunk, int volume);
extern DECLSPEC int  Mix_VolumeMusic(int volume);
extern DECLSPEC int  Mix_HaltChannel(int channel);
extern DECLSPEC int  Mix_HaltGroup(int tag);
extern DECLSPEC int  Mix_HaltMusic();
extern DECLSPEC int  Mix_ExpireChannel(int channel, int ticks);
extern DECLSPEC int  Mix_FadeOutChannel(int which, int ms);
extern DECLSPEC int  Mix_FadeOutGroup(int tag, int ms);
extern DECLSPEC int  Mix_FadeOutMusic(int ms);
extern DECLSPEC Mix_Fading  Mix_FadingMusic();
extern DECLSPEC Mix_Fading  Mix_FadingChannel(int which);
extern DECLSPEC void  Mix_Pause(int channel);
extern DECLSPEC void  Mix_Resume(int channel);
extern DECLSPEC int  Mix_Paused(int channel);
extern DECLSPEC void  Mix_PauseMusic();
extern DECLSPEC void  Mix_ResumeMusic();
extern DECLSPEC void  Mix_RewindMusic();
extern DECLSPEC int  Mix_PausedMusic();
extern DECLSPEC int  Mix_SetMusicPosition(double position);
extern DECLSPEC int  Mix_Playing(int channel);
extern DECLSPEC int  Mix_PlayingMusic();
extern DECLSPEC int  Mix_SetMusicCMD(const char *command);
extern DECLSPEC int  Mix_SetSynchroValue(int value);
extern DECLSPEC int  Mix_GetSynchroValue();
extern DECLSPEC Mix_Chunk *  Mix_GetChunk(int channel);
extern DECLSPEC void  Mix_CloseAudio();
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
//#define Mix_FadeInChannel(channel,chunk,loops,ms) Mix_FadeInChannelTimed(channel,chunk,loops,ms,-1)
#LISP (progn
	(export 'Mix_FadeInChannel)
	(defun Mix_FadeInChannel (channel chunk loops ms)
        (Mix_FadeInChannelTimed channel chunk loops ms -1)))
!#

#! (:export t :library "SDL_mixer" :ignore "DECLSPEC")
//#define Mix_SetError	SDL_SetError
//#LISP (progn
//	(export 'Mix_SetError)
//	(defun Mix_SetError ()
//        (SDL_SetError)))

//#define Mix_GetError	SDL_GetError
#LISP (progn
	(export 'Mix_GetError)
	(defun Mix_GetError ()
        (SDL_GetError)))
!#

(provide "SDL-mixer")

;;(ccl:set-current-directory "C:/Documents and Settings/00u4440/My Documents/dev/corman-sdl/ffi")
;;(ccl:set-current-directory "F:/Documents and Settings/Crook/My Documents/dev/corman-sdl/corman-sdl/ffi")

;;(ct:transcribe-file "sdl_mixer_h.lisp" "sdl_mixer.lisp" :common-lisp-user nil)