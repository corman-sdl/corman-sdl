;;;;; SDL bindings for Corman Lisp
;;;;; Version 0.6.2, 2004/06/04
;;;;; Author: Luke Crook ( luke@balooga.com )
;;;;; SDL Version 1.2.7.0
;;;;; Corman Lisp Version 2.5
;;;;; 
;;;;;	Completed
;;;;;		"begin_code.h"
;;;;;			Nothing to do. Complete.
;;;;;
;;;;;		"close_code.h"
;;;;;			Nothing to do. Complete.
;;;;;
;;;;;		"sdl_name.h"
;;;;;			TODO:
;;;;;               #define SDL_NAME(X)	SDL_##X
;;;;;
;;;;;		"sdl_copying.h"
;;;;;			SDL_copying.h,v 1.5 2004/01/04 16:49:07 slouken 
;;;;;           Nothing to do. Complete.
;;;;;
;;;;;		"sdl_types.h"
;;;;;			SDL_types.h,v 1.11 2004/01/04 16:49:08 slouken
;;;;;           NOTE:
;;;;;               Unsure if the 64 bit type is correctly supported.
;;;;;               No compile time assertions. (Not required).
;;;;;			TODO:
;;;;;				SDL_TABLESIZE(table)
;;;;;
;;;;;		"sdl_cpuinfo.h"
;;;;;			SDL_cpuinfo.h,v 1.5 2004/01/24 05:47:18 slouken 
;;;;;           complete
;;;;;
;;;;;		"sdl_version.h"
;;;;;			SDL_version.h,v 1.12 2004/01/04 16:49:08 slouken
;;;;;           Modifications:
;;;;;               #define SDL_VERSION(X) changed to +SDL_VERSION+ because of possible
;;;;;                   name clash with the SDL_Version struct
;;;;;           Complete.
;;;;;
;;;;;		"sdl_syswm.h"
;;;;;			SDL_syswm.h,v 1.12 2004/01/04 16:49:08 slouken 
;;;;;           Complete.
;;;;;               
;;;;;		"sdl_rwops.h"
;;;;;			SDL_rwops.h,v 1.7 2004/01/04 16:49:07 slouken
;;;;;           TODO:
;;;;;               //extern DECLSPEC SDL_RWops * SDL_RWFromFP(FILE *fp, int autoclose);
;;;;;               //#define SDL_RWseek(ctx, offset, whence)	(ctx)->seek(ctx, offset, whence)
;;;;;               //#define SDL_RWtell(ctx)			(ctx)->seek(ctx, 0, SEEK_CUR)
;;;;;               //#define SDL_RWread(ctx, ptr, size, n)	(ctx)->read(ctx, ptr, size, n)
;;;;;               //#define SDL_RWwrite(ctx, ptr, size, n)	(ctx)->write(ctx, ptr, size, n)
;;;;;               //#define SDL_RWclose(ctx)		(ctx)->close(ctx)
;;;;;               
;;;;;               Unions are not supported in CCL therefore, created three additional structs in case 
;;;;;               the variables within the "hidden" union need to be accessed. Otherwise,
;;;;;               just use SDL_RWops. The hidden structs are:
;;;;;                   SDL_RWops_stdio
;;;;;                   SDL_RWops_mem
;;;;;                   SDL_RWops_unknown
;;;;;           MODIFICATIONS:
;;;;;               Replaced:
;;;;;                   
;;;;;                 	union {
;;;;;              	        struct {
;;;;;                          int autoclose;
;;;;;                          FILE *fp;
;;;;;              	        } stdio;
;;;;;               	    struct {
;;;;;                          Uint8 *base;
;;;;;                          Uint8 *here;
;;;;;                          Uint8 *stop;
;;;;;               	    } mem;
;;;;;               	    struct {
;;;;;                          void *data1;
;;;;;               	    } unknown;
;;;;;               	} hidden;
;;;;;               
;;;;;               with this:
;;;;;                   Uint32 buffer[4];
;;;;;               
;;;;;		"sdl.h"
;;;;;			Id: SDL.h,v 1.7 2002/04/11 14:35:13 slouken
;;;;;			Complete
;;;;;
;;;;;		"sdl_main.h"   
;;;;;			SDL_main.h,v 1.12 2004/01/04 16:49:07 slouken 
;;;;;			Complete
;;;;;
;;;;;		"sdl_error.h"   
;;;;;			SDL_error.h,v 1.6 2004/01/04 16:49:07 slouken
;;;;;			NOTE:
;;;;;               The Corman Lisp FFI does not easily support variable arguments.
;;;;;               Therefore expect SDL_SetError to be supported around the '1st of Never
;;;;;			TODO:
;;;;;				SDLCALL SDL_SetError(const char *fmt, ...)
;;;;;
;;;;;		"sdl_mutex.h"
;;;;;           SDL_mutex.h,v 1.7 2004/01/04 16:49:07 slouken 
;;;;;           Complete.
;;;;;
;;;;;		"sdl_thread.h"
;;;;;          SDL_thread.h,v 1.6 2004/01/04 16:49:08 slouken
;;;;;           Use the threads package provided by Chris Double, or raw OS threads 
;;;;;           provided by Corman Lisp. Easier to use than SDL threads.
;;;;;           NOTE:
;;;;;               It is possible to use the mutex and semaphores provided by SDL 
;;;;;               in sdl_mutex.h with Corman threads.
;;;;;           MODIFICATIONS:
;;;;;               Changed:
;;;;;               extern DECLSPEC SDL_Thread * SDLCALL SDL_CreateThread(int (*fn)(void *), void *data);
;;;;;               To:
;;;;;               typedef int (*threadfn)(void *);
;;;;;               extern DECLSPEC SDL_Thread * SDL_CreateThread(threadfn fn, void *data);
;;;;;           TODO:
;;;;;			    SDL_CreateThread callback is not yet working (Similar problem to AddTimer). 
;;;;;               Therefore it is impossible to create a SDL thread. 
;;;;;               Use Corman / Chris Double's threads instead.
;;;;;               
;;;;;		"sdl_video.h"
;;;;;			SDL_video.h,v 1.17 2004/01/04 16:49:08 slouken	
;;;;;           NOTE:
;;;;;				SDL_SoftStretch - not yet in public API
;;;;;           MODIFICATIONS:
;;;;;               Changed:
;;;;;               typedef int (*SDL_blit)(struct SDL_Surface *src, SDL_Rect *srcrect,
;;;;;                   struct SDL_Surface *dst, SDL_Rect *dstrect);
;;;;;                   To:
;;;;;               typedef int (*SDL_blit)(SDL_Surface *src, SDL_Rect *srcrect,
;;;;;                   SDL_Surface *dst, SDL_Rect *dstrect);
;;;;;               #define SDL_Colour SDL_Color
;;;;;                   To:
;;;;;               typedef struct {
;;;;;               	Uint8 r;
;;;;;               	Uint8 g;
;;;;;               	Uint8 b;
;;;;;               	Uint8 unused;
;;;;;               } SDL_Colour
;;;;;			TODO:
;;;;;				SDL_VideoInfo - the following bit fields are not defined
;;;;;                   Uint32 hw_available :1;	/* Flag: Can you create hardware surfaces? */
;;;;;                   Uint32 wm_available :1;	/* Flag: Can you talk to a window manager? */
;;;;;                   Uint32 blit_hw      :1;	/* Flag: Accelerated blits HW --> HW */
;;;;;                   Uint32 blit_hw_CC   :1;	/* Flag: Accelerated blits with Colorkey */
;;;;;                   Uint32 blit_hw_A    :1;	/* Flag: Accelerated blits with Alpha */
;;;;;                   Uint32 blit_sw      :1;	/* Flag: Accelerated blits SW --> HW */
;;;;;                   Uint32 blit_sw_CC   :1;	/* Flag: Accelerated blits with Colorkey */
;;;;;                   Uint32 blit_sw_A    :1;	/* Flag: Accelerated blits with Alpha */
;;;;;                   Uint32 blit_fill    :1;	/* Flag: Accelerated color fill */
;;;;;				SDL_Overlay - the following bit fields are not defined
;;;;;                   int32 hw_overlay :1;	/* Flag: This overlay hardware accelerated? */
;;;;;                       (added Uint32 TEMP;)
;;;;;
;;;;;		"SDL_active.h"
;;;;;           SDL_active.h,v 1.6 2004/01/04 16:49:07 slouken 
;;;;;			Complete
;;;;;
;;;;;		"SDL_keysym.h"
;;;;;			SDL_keysym.h,v 1.6 2004/01/04 16:49:07 slouken 
;;;;;			Complete
;;;;;
;;;;;		"SDL_keyboard.h"
;;;;;			SDL_keyboard.h,v 1.6 2004/01/04 16:49:07 slouken 
;;;;;			Complete
;;;;;
;;;;;		"SDL_mouse.h"
;;;;;			SDL_mouse.h,v 1.7 2004/01/04 16:49:07 slouken 
;;;;;			Complete
;;;;;
;;;;;		"SDL_joystick.h"
;;;;;			SDL_joystick.h,v 1.6 2004/01/04 16:49:07 slouken
;;;;;			Complete
;;;;;
;;;;;		"SDL_events.h"
;;;;;			SDL_events.h,v 1.9 2004/02/18 03:57:13 slouken 
;;;;;           NOTE:
;;;;;               The C to Lisp translator does not support unions. And as I am too 
;;;;;               lazy to determine the exact largest size for the SDL_Event struct,
;;;;;               we shall create a struct that we hope is large enough to hold any event.
;;;;;               If something unexplained happens during runtime, e.g. weird crashes etc.
;;;;;               then take a look at this struct, it may be causing the problem.
;;;;;           Complete.
;;;;;
;;;;;		"SDL_quit.h"
;;;;;			SDL_quit.h,v 1.5 2004/01/04 16:49:07 slouken 
;;;;;			TODO:
;;;;;               #define SDL_QuitRequested() \
;;;;;                   (SDL_PumpEvents(), SDL_PeepEvents(NULL,0,SDL_PEEKEVENT,SDL_QUITMASK))            
;;;;;
;;;;;		"SDL_timer.h"
;;;;;           SDL_timer.h,v 1.6 2004/01/04 16:49:08 slouken
;;;;;			TODO:
;;;;;               AddTimer callback does not yet work.
;;;;;                   extern DECLSPEC SDL_TimerID SDL_AddTimer(Uint32 interval, SDL_NewTimerCallback callback, void *param);
;;;;;               
;;;;;		"SDL_getenv.h"
;;;;;           
;;;;;           Complete
;;;;;				
;;;;;		"SDL_endian.h"
;;;;;           SDL_endian.h,v 1.6 2004/01/04 16:49:07 slouken 
;;;;;			Not yet implemented.
;;;;;				
;;;;;		"SDL_byteorder.h"
;;;;;           SDL_byteorder.h,v 1.8 2004/01/04 16:49:07 slouken 
;;;;;			NOTE: Hardwired Little endianess (Intel)
;;;;;           Complete.
;;;;;
;;;;;		"SDL_cdrom.h"
;;;;;           SDL_cdrom.h,v 1.6 2004/01/04 16:49:07 slouken
;;;;;           MODIFICATIONS:
;;;;;               Changed SDL_CDtrack track[SDL_MAX_TRACKS+1] to SDL_CDtrack track[100];
;;;;;			Complete.
;;;;;
;;;;;		"SDL_audio.h"
;;;;;           SDL_audio.h,v 1.7 2004/01/04 16:49:07 slouken
;;;;;           TODO:
;;;;;               typedef struct SDL_AudioCVT
;;;;;                   specifically: void (*filters[10])(struct SDL_AudioCVT *cvt, Uint16 format);
;;;;;
;;;;;		"SDL_loadso.h"
;;;;;           SDL_loadso.h,v 1.5 2004/01/04 16:49:07 slouken 
;;;;;           Complete.
;;;;;				
;;;;;				
;;;;;		"SDL_opengl.h"
;;;;;           Not included.
;;;;;           See opengl__h. Corman Common Lisp already has partial bindings to OpenGL
;;;;;			

;;(require :win32)
;(make-package "SDL" :use '("COMMON-LISP" "C-TYPES" "WIN32"));
;(in-package :SDL)



;(require :windef)
;(in-package :win)

(require :windef)
(in-package :SDL)




;(defpackage #:sdl
;  (:use #:windef)
;  (:export #:sdl)
;  (:documentation "Simple DirectMedia Layer binding for Corman Common Lisp"))

;(in-package #:sdl)

;; "begin_code.h"
;;

;; "close_code.h"
;; 

;; "sdl_name.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_NAME(X)	SDL_##X
!#

;; "sdl_types.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* The number of elements in a table */
//#define SDL_TABLESIZE(table)	(sizeof(table)/sizeof(table[0]))

typedef enum {
	SDL_FALSE = 0,
	SDL_TRUE  = 1
} SDL_bool;

//#LISP (ct:defctype Uint8 :unsigned-char)
//#LISP (ct:defctype Sint8 :char)
//#LISP (ct:defctype Uint16 :unsigned-short)
//#LISP (ct:defctype Sint16 :short)
//#LISP (ct:defctype Uint32 :unsigned-long)
//#LISP (ct:defctype Sint32 :long)
#LISP (ct:defctype VOID :void)

typedef unsigned char	Uint8;
typedef signed char	    Sint8;
typedef unsigned short	Uint16;
typedef signed short	Sint16;
typedef unsigned int	Uint32;
typedef signed int	    Sint32;

typedef struct {
	Uint32 hi;
	Uint32 lo;
} Uint64, Sint64;

/* General keyboard/mouse state definitions */
enum { SDL_PRESSED = 0x01, SDL_RELEASED = 0x00 };
!#


;; "sdl_cpuinfo.h"
;;

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC SDL_bool SDL_HasRDTSC();
extern DECLSPEC SDL_bool SDL_HasMMX();
extern DECLSPEC SDL_bool SDL_HasMMXExt();
extern DECLSPEC SDL_bool SDL_Has3DNow();
extern DECLSPEC SDL_bool SDL_Has3DNowExt();
extern DECLSPEC SDL_bool SDL_HasSSE();
extern DECLSPEC SDL_bool SDL_HasSSE2();
extern DECLSPEC SDL_bool SDL_HasAltiVec();
!#

;; "sdl_version.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_MAJOR_VERSION	1
#define SDL_MINOR_VERSION	2
#define SDL_PATCHLEVEL		7

typedef struct {
	Uint8 major;
	Uint8 minor;
	Uint8 patch;
} SDL_version;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/*
#define SDL_VERSION(X)							\
{									\
	(X)->major = SDL_MAJOR_VERSION;					\
	(X)->minor = SDL_MINOR_VERSION;					\
	(X)->patch = SDL_PATCHLEVEL;					\
}
*/
#LISP (progn
	(export '+SDL_VERSION+)
	(defun +SDL_VERSION+ (x)
        (setf (ct:cref SDL_version x major) SDL_MAJOR_VERSION)
        (setf (ct:cref SDL_version x minor) SDL_MINOR_VERSION)
        (setf (ct:cref SDL_version x patch) SDL_PATCHLEVEL)))

!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_VERSIONNUM(X, Y, Z)						\
//	(X)*1000 + (Y)*100 + (Z)
#LISP (progn
	(export 'SDL_VERSIONNUM)
	(defun SDL_VERSIONNUM (x y z)
        (+
            (* x 1000)
            (* y 100)
            z)))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_COMPILEDVERSION \
//	SDL_VERSIONNUM(SDL_MAJOR_VERSION, SDL_MINOR_VERSION, SDL_PATCHLEVEL)
#LISP (progn
	(export 'SDL_COMPILEDVERSION)
	(defun SDL_COMPILEDVERSION ()
        (SDL_VERSIONNUM SDL_MAJOR_VERSION SDL_MINOR_VERSION SDL_PATCHLEVEL)))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_VERSION_ATLEAST(X, Y, Z) \
//	(SDL_COMPILEDVERSION >= SDL_VERSIONNUM(X, Y, Z))
#LISP (progn
	(export 'SDL_VERSION_ATLEAST)
	(defun SDL_VERSION_ATLEAST (x y z)
        (if (>= (SDL_COMPILEDVERSION) (SDL_VERSIONNUM x y z))
            1
            0)))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC const SDL_version * SDL_Linked_Version();
!#

;; "sdl_syswm.h"
;; 


#! (:export t :library "KERNEL32")
typedef unsigned int UINT;
!#

#! (:export t :library "KERNEL32")
typedef HANDLE HWND;
typedef UINT WPARAM;
//typedef LONG LPARAM;
typedef unsigned int LPARAM;
typedef HANDLE HGLRC;
!#

#! (:export t :library "SDL")
struct SDL_SysWMinfo;
//typedef struct SDL_SysWMinfo SDL_SysWMinfo;
!#

;; For some reason, (ct:transcribe-file) chokes on the following 
;; two structs.

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
struct SDL_SysWMmsg {
	SDL_version version;
	HWND hwnd;			/* The window for the message */
	UINT msg;			/* The type of message */
	WPARAM wParam;			/* WORD message parameter */
	LPARAM lParam;			/* LONG message parameter */
};
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef struct SDL_SysWMinfo {
	SDL_version version;
	HWND window;			/* The Win32 display window */
	HGLRC hglrc;			/* The OpenGL context, if any */
} SDL_SysWMinfo;
!#


#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_GetWMInfo(SDL_SysWMinfo *info);
!#


;; "sdl_rwops.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef struct SDL_RWops SDL_RWops;
//int (*seek)(struct SDL_RWops *context, int offset, int whence);
//typedef int (*seek_p)(SDL_RWops *context, int offset, int whence);
//int (*read)(struct SDL_RWops *context, void *ptr, int size, int maxnum);
//typedef int (*read_p)(SDL_RWops *context, void *ptr, int size, int maxnum);
//int (*write)(struct SDL_RWops *context, const void *ptr, int size, int num);
//typedef int (*write_p)(SDL_RWops *context, const void *ptr, int size, int num);
//int (*close)(struct SDL_RWops *context);
//typedef int (*close_p)(SDL_RWops *context);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef struct SDL_RWops {
//	int (*seek)(struct SDL_RWops *context, int offset, int whence);
	int (*seek)(SDL_RWops *context, int offset, int whence);
//	int (*read)(struct SDL_RWops *context, void *ptr, int size, int maxnum);
	int (*read)(SDL_RWops *context, void *ptr, int size, int maxnum);
//	int (*write)(struct SDL_RWops *context, const void *ptr, int size, int num);
	int (*write)(SDL_RWops *context, const void *ptr, int size, int num);
//	int (*close)(struct SDL_RWops *context);
	int (*close)(SDL_RWops *context);

	Uint32 type;
/*
   	union {
	    struct {
            int autoclose;
            FILE *fp;
	    } stdio;
	    struct {
            Uint8 *base;
            Uint8 *here;
            Uint8 *stop;
	    } mem;
	    struct {
            void *data1;
	    } unknown;
	} hidden;
*/
    Uint32 buffer[4];
} SDL_RWops;

typedef struct SDL_RWops_stdio {
//	int (*seek)(struct SDL_RWops *context, int offset, int whence);
	int (*seek)(SDL_RWops *context, int offset, int whence);
//	int (*read)(struct SDL_RWops *context, void *ptr, int size, int maxnum);
	int (*read)(SDL_RWops *context, void *ptr, int size, int maxnum);
//	int (*write)(struct SDL_RWops *context, const void *ptr, int size, int num);
	int (*write)(SDL_RWops *context, const void *ptr, int size, int num);
//	int (*close)(struct SDL_RWops *context);
	int (*close)(SDL_RWops *context);

	Uint32 type;
//	struct {
        int autoclose;
//        FILE *fp;
        void *fp;
//	} stdio;
} SDL_RWops_stdio;

typedef struct SDL_RWops_mem {
//	int (*seek)(struct SDL_RWops *context, int offset, int whence);
	int (*seek)(SDL_RWops *context, int offset, int whence);
//	int (*read)(struct SDL_RWops *context, void *ptr, int size, int maxnum);
	int (*read)(SDL_RWops *context, void *ptr, int size, int maxnum);
//	int (*write)(struct SDL_RWops *context, const void *ptr, int size, int num);
	int (*write)(SDL_RWops *context, const void *ptr, int size, int num);
//	int (*close)(struct SDL_RWops *context);
	int (*close)(SDL_RWops *context);

	Uint32 type;
/*
//    struct {
        Uint8 *base;
        Uint8 *here;
        Uint8 *stop;
//    } mem;
*/
} SDL_RWops_mem;

typedef struct SDL_RWops_unknown {
//	int (*seek)(struct SDL_RWops *context, int offset, int whence);
	int (*seek)(SDL_RWops *context, int offset, int whence);
//	int (*read)(struct SDL_RWops *context, void *ptr, int size, int maxnum);
	int (*read)(SDL_RWops *context, void *ptr, int size, int maxnum);
//	int (*write)(struct SDL_RWops *context, const void *ptr, int size, int num);
	int (*write)(SDL_RWops *context, const void *ptr, int size, int num);
//	int (*close)(struct SDL_RWops *context);
	int (*close)(SDL_RWops *context);

	Uint32 type;
//    struct {
        void *data1;
//    } unknown;
} SDL_RWops_unknown;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC SDL_RWops * SDL_RWFromFile(const char *file, const char *mode);
//extern DECLSPEC SDL_RWops * SDL_RWFromFP(FILE *fp, int autoclose);
extern DECLSPEC SDL_RWops * SDL_RWFromMem(void *mem, int size);
extern DECLSPEC SDL_RWops * SDL_RWFromConstMem(const void *mem, int size);
extern DECLSPEC SDL_RWops * SDL_AllocRW();
extern DECLSPEC void SDL_FreeRW(SDL_RWops *area);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/*
//#define SDL_RWseek(ctx, offset, whence)	(ctx)->seek(ctx, offset, whence)
//#define SDL_RWtell(ctx)			(ctx)->seek(ctx, 0, SEEK_CUR)
//#define SDL_RWread(ctx, ptr, size, n)	(ctx)->read(ctx, ptr, size, n)
//#define SDL_RWwrite(ctx, ptr, size, n)	(ctx)->write(ctx, ptr, size, n)
//#define SDL_RWclose(ctx)		(ctx)->close(ctx)
*/
!#

;; 	"sdl.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define	SDL_INIT_TIMER		0x00000001
#define SDL_INIT_AUDIO		0x00000010
#define SDL_INIT_VIDEO		0x00000020
#define SDL_INIT_CDROM		0x00000100
#define SDL_INIT_JOYSTICK	0x00000200
#define SDL_INIT_NOPARACHUTE	0x00100000	/* Don't catch fatal signals */
#define SDL_INIT_EVENTTHREAD	0x01000000	/* Not supported on all OS's */
#define SDL_INIT_EVERYTHING	0x0000FFFF
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_Init(Uint32 flags);
extern DECLSPEC int SDL_InitSubSystem(Uint32 flags);
extern DECLSPEC void SDL_QuitSubSystem(Uint32 flags);
extern DECLSPEC Uint32 SDL_WasInit(Uint32 flags);
extern DECLSPEC void SDL_Quit();
!#

;; 	"sdl_main.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC void SDL_SetModuleHandle(void *hInst);
extern DECLSPEC int SDL_RegisterApp(char *name, Uint32 style, void *hInst);
!#

;; 	"sdl_error.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//extern DECLSPEC void SDL_SetError(const char *fmt, ...);
extern DECLSPEC char * SDL_GetError();
extern DECLSPEC void SDL_ClearError();
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef enum {
	SDL_ENOMEM,
	SDL_EFREAD,
	SDL_EFWRITE,
	SDL_EFSEEK,
	SDL_LASTERROR
} SDL_errorcode;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern void SDL_Error(SDL_errorcode code);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* Private error message function - used internally */
//#define SDL_OutOfMemory()	SDL_Error(SDL_ENOMEM)
#LISP (progn
	(export 'SDL_OutOfMemory)
	(defun SDL_OutOfMemory ()
            (SDL_Error SDL_ENOMEM)))
!#



;; 	"sdl_mutex.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_MUTEX_TIMEDOUT	1

//#define SDL_MUTEX_MAXWAIT	(~(Uint32)0)
#LISP (win:defwinconstant SDL_MUTEX_MAXWAIT (lognot 0))

typedef struct SDL_mutex {};
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC SDL_mutex * SDL_CreateMutex();
extern DECLSPEC int SDL_mutexP(SDL_mutex *mutex);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_LockMutex(m)	SDL_mutexP(m)
#LISP (progn
	(export 'SDL_LockMutex)
	(defun SDL_LockMutex (m)
            (SDL_mutexP m)))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_mutexV(SDL_mutex *mutex);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_UnlockMutex(m)	SDL_mutexV(m)
#LISP (progn
	(export 'SDL_UnlockMutex)
	(defun SDL_UnlockMutex (m)
            (SDL_mutexV m)))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC void SDL_DestroyMutex(SDL_mutex *mutex);

typedef struct SDL_semaphore SDL_sem;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC SDL_sem * SDL_CreateSemaphore(Uint32 initial_value);
extern DECLSPEC void SDL_DestroySemaphore(SDL_sem *sem);
extern DECLSPEC int SDL_SemWait(SDL_sem *sem);
extern DECLSPEC int SDL_SemTryWait(SDL_sem *sem);
extern DECLSPEC int SDL_SemWaitTimeout(SDL_sem *sem, Uint32 ms);
extern DECLSPEC int SDL_SemPost(SDL_sem *sem);
extern DECLSPEC Uint32 SDL_SemValue(SDL_sem *sem);

typedef struct SDL_cond {};
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC SDL_cond * SDL_CreateCond();
extern DECLSPEC void SDL_DestroyCond(SDL_cond *cond);
extern DECLSPEC int SDL_CondSignal(SDL_cond *cond);
extern DECLSPEC int SDL_CondBroadcast(SDL_cond *cond);
extern DECLSPEC int SDL_CondWait(SDL_cond *cond, SDL_mutex *mut);
extern DECLSPEC int SDL_CondWaitTimeout(SDL_cond *cond, SDL_mutex *mutex, Uint32 ms);
!#

;; 	"sdl_thread.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef struct SDL_Thread {};
!#

;; Added the following callback function prototype
#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef int (*threadfn)(void *);
!#

;; Modified SDL_CreateThread to accept the function prototype
#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//extern DECLSPEC SDL_Thread * SDLCALL SDL_CreateThread(int (*fn)(void *), void *data);
extern DECLSPEC SDL_Thread * SDL_CreateThread(threadfn fn, void *data);
extern DECLSPEC Uint32 SDL_ThreadID();
extern DECLSPEC Uint32 SDL_GetThreadID(SDL_Thread *thread);
extern DECLSPEC void SDL_WaitThread(SDL_Thread *thread, int *status);
extern DECLSPEC void SDL_KillThread(SDL_Thread *thread);
!#

;; 	"sdl_video.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_ALPHA_OPAQUE 255
#define SDL_ALPHA_TRANSPARENT 0
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef struct {
	Sint16 x;
	Sint16 y;
	Uint16 w;
	Uint16 h;
} SDL_Rect;

typedef struct {
	Uint8 r;
	Uint8 g;
	Uint8 b;
	Uint8 unused;
} SDL_Color;
//#define SDL_Colour SDL_Color

typedef struct {
	Uint8 r;
	Uint8 g;
	Uint8 b;
	Uint8 unused;
} SDL_Colour;

typedef struct {
	int       ncolors;
	SDL_Color *colors;
} SDL_Palette;

typedef struct SDL_PixelFormat {
	SDL_Palette *palette;
	Uint8  BitsPerPixel;
	Uint8  BytesPerPixel;
	Uint8  Rloss;
	Uint8  Gloss;
	Uint8  Bloss;
	Uint8  Aloss;
	Uint8  Rshift;
	Uint8  Gshift;
	Uint8  Bshift;
	Uint8  Ashift;
	Uint32 Rmask;
	Uint32 Gmask;
	Uint32 Bmask;
	Uint32 Amask;

	/* RGB color key information */
	Uint32 colorkey;
	/* Alpha value information (per-surface alpha) */
	Uint8  alpha;
} SDL_PixelFormat;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* typedef for private surface blitting functions */
struct SDL_Surface;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//typedef int (*SDL_blit)(struct SDL_Surface *src, SDL_Rect *srcrect,
//			struct SDL_Surface *dst, SDL_Rect *dstrect);
typedef int (*SDL_blit)(SDL_Surface *src, SDL_Rect *srcrect,
			SDL_Surface *dst, SDL_Rect *dstrect);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
struct private_hwdata {};
struct SDL_BlitMap {};
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef struct SDL_Surface {
	Uint32 flags;				/* Read-only */
	SDL_PixelFormat *format;		/* Read-only */
	int w;
	int h;				/* Read-only */
	Uint16 pitch;				/* Read-only */
	void *pixels;				/* Read-write */
	int offset;				/* Private */

	/* Hardware-specific surface info */
	private_hwdata *hwdata;
	/* clipping information */
	SDL_Rect clip_rect;			/* Read-only */
	Uint32 unused1;				/* for binary compatibility */

	/* Allow recursive locks */
	Uint32 locked;				/* Private */

	/* info for fast blit mapping to other surfaces */
	SDL_BlitMap *map;		/* Private */

	/* format version, bumped at every change to invalidate blit maps */
	unsigned int format_version;		/* Private */

	/* Reference count -- used when freeing surface */
	int refcount;				/* Read-mostly */
} SDL_Surface;

#define SDL_SWSURFACE	0x00000000	/* Surface is in system memory */
#define SDL_HWSURFACE	0x00000001	/* Surface is in video memory */
#define SDL_ASYNCBLIT	0x00000004	/* Use asynchronous blits if possible */
#define SDL_ANYFORMAT	0x10000000	/* Allow any video depth/pixel-format */
#define SDL_HWPALETTE	0x20000000	/* Surface has exclusive palette */
#define SDL_DOUBLEBUF	0x40000000	/* Set up double-buffered video mode */
#define SDL_FULLSCREEN	0x80000000	/* Surface is a full screen display */
#define SDL_OPENGL      0x00000002      /* Create an OpenGL rendering context */
#define SDL_OPENGLBLIT	0x0000000A	/* Create an OpenGL rendering context and use it for blitting */
#define SDL_RESIZABLE	0x00000010	/* This video mode may be resized */
#define SDL_NOFRAME	0x00000020	/* No window caption or edge frame */
#define SDL_HWACCEL	0x00000100	/* Blit uses hardware acceleration */
#define SDL_SRCCOLORKEY	0x00001000	/* Blit uses a source color key */
#define SDL_RLEACCELOK	0x00002000	/* Private flag */
#define SDL_RLEACCEL	0x00004000	/* Surface is RLE encoded */
#define SDL_SRCALPHA	0x00010000	/* Blit uses source alpha blending */
#define SDL_PREALLOC	0x01000000	/* Surface uses preallocated memory */
!#

#! (:library "SDL" :export t :ignore "DECLSPEC")
//#define SDL_MUSTLOCK(surface) (surface->offset || ((surface->flags & (SDL_HWSURFACE|SDL_ASYNCBLIT|SDL_RLEACCEL)) != 0))
#LISP (progn
	(export 'SDL_MUSTLOCK)
    (declaim (inline SDL_MUSTLOCK))
    (defun SDL_MUSTLOCK (surface)
        (if (> 0 (ct:cref SDL_Surface surface offset))
            (values 1)
            (if (not (eql 0 (logand 
                            (ct:cref SDL_Surface surface flags)
                            (logior SDL_HWSURFACE SDL_ASYNCBLIT SDL_RLEACCEL))))
                (values 1)
                (values 0)))))
!#

#! (:library "SDL" :export t :ignore "DECLSPEC")
/* Useful for determining the video hardware capabilities */
//typedef struct {
//	Uint32 hw_available :1;	/* Flag: Can you create hardware surfaces? */
//	Uint32 wm_available :1;	/* Flag: Can you talk to a window manager? */
//	Uint32 UnusedBits1  :6;
//	Uint32 UnusedBits2  :1;
//	Uint32 blit_hw      :1;	/* Flag: Accelerated blits HW --> HW */
//	Uint32 blit_hw_CC   :1;	/* Flag: Accelerated blits with Colorkey */
//	Uint32 blit_hw_A    :1;	/* Flag: Accelerated blits with Alpha */
//	Uint32 blit_sw      :1;	/* Flag: Accelerated blits SW --> HW */
//	Uint32 blit_sw_CC   :1;	/* Flag: Accelerated blits with Colorkey */
//	Uint32 blit_sw_A    :1;	/* Flag: Accelerated blits with Alpha */
//	Uint32 blit_fill    :1;	/* Flag: Accelerated color fill */
//	Uint32 UnusedBits3  :16;
//	Uint32 video_mem;	/* The total amount of video memory (in K) */
//	SDL_PixelFormat *vfmt;	/* Value: The format of the video surface */
//} SDL_VideoInfo;
!#

#! (:library "SDL" :export t :ignore "DECLSPEC" :pack 4 )
typedef struct {
	Uint8 int1;
	Uint8 int2;
	Uint16 int3;
    Uint32 video_mem;
    SDL_PixelFormat *vfmt;
} SDL_VideoInfo;
!#

#! (:library "SDL" :export t :ignore "DECLSPEC")
#define SDL_YV12_OVERLAY  0x32315659	/* Planar mode: Y + V + U  (3 planes) */
#define SDL_IYUV_OVERLAY  0x56555949	/* Planar mode: Y + U + V  (3 planes) */
#define SDL_YUY2_OVERLAY  0x32595559	/* Packed mode: Y0+U0+Y1+V0 (1 plane) */
#define SDL_UYVY_OVERLAY  0x59565955	/* Packed mode: U0+Y0+V0+Y1 (1 plane) */
#define SDL_YVYU_OVERLAY  0x55595659	/* Packed mode: Y0+V0+Y1+U0 (1 plane) */

struct private_yuvhwfuncs {};
struct private_yuvhwdata {};

/* The YUV hardware video overlay */
typedef struct SDL_Overlay {
	Uint32 format;				/* Read-only */
	int w;
	int h;				/* Read-only */
	int planes;				/* Read-only */
	Uint16 *pitches;			/* Read-only */
	Uint8 **pixels;				/* Read-write */

	/* Hardware-specific surface info */
//	struct private_yuvhwfuncs *hwfuncs;
	private_yuvhwfuncs *hwfuncs;
//	struct private_yuvhwdata *hwdata;
	private_yuvhwdata *hwdata;

	/* Special flags */
//	Uint32 hw_overlay :1;	/* Flag: This overlay hardware accelerated? */
//	Uint32 UnusedBits :31;
    Uint32 TEMP;
} SDL_Overlay;

/* Public enumeration for setting the OpenGL window attributes. */
typedef enum {
    SDL_GL_RED_SIZE,
    SDL_GL_GREEN_SIZE,
    SDL_GL_BLUE_SIZE,
    SDL_GL_ALPHA_SIZE,
    SDL_GL_BUFFER_SIZE,
    SDL_GL_DOUBLEBUFFER,
    SDL_GL_DEPTH_SIZE,
    SDL_GL_STENCIL_SIZE,
    SDL_GL_ACCUM_RED_SIZE,
    SDL_GL_ACCUM_GREEN_SIZE,
    SDL_GL_ACCUM_BLUE_SIZE,
    SDL_GL_ACCUM_ALPHA_SIZE,
	SDL_GL_STEREO,
    SDL_GL_MULTISAMPLEBUFFERS,
    SDL_GL_MULTISAMPLESAMPLES
} SDL_GLattr;

/* flags for SDL_SetPalette() */
#define SDL_LOGPAL 0x01
#define SDL_PHYSPAL 0x02
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_VideoInit(const char *driver_name, Uint32 flags);
extern DECLSPEC void SDL_VideoQuit();
extern DECLSPEC char * SDL_VideoDriverName(char *namebuf, int maxlen);
extern DECLSPEC SDL_Surface * SDL_GetVideoSurface();
extern DECLSPEC const SDL_VideoInfo * SDL_GetVideoInfo();
extern DECLSPEC int SDL_VideoModeOK(int width, int height, int bpp, Uint32 flags);
extern DECLSPEC SDL_Rect ** SDL_ListModes(SDL_PixelFormat *format, Uint32 flags);
extern DECLSPEC SDL_Surface * SDL_SetVideoMode(int width, int height, int bpp, Uint32 flags);
extern DECLSPEC void SDL_UpdateRects(SDL_Surface *screen, int numrects, SDL_Rect *rects);
extern DECLSPEC void SDL_UpdateRect(SDL_Surface *screen, Sint32 x, Sint32 y, Uint32 w, Uint32 h);
extern DECLSPEC int SDL_Flip(SDL_Surface *screen);
extern DECLSPEC int SDL_SetGamma(float red, float green, float blue);
extern DECLSPEC int SDL_SetGammaRamp(Uint16 *red, Uint16 *green, Uint16 *blue);
extern DECLSPEC int SDL_GetGammaRamp(Uint16 *red, Uint16 *green, Uint16 *blue);
extern DECLSPEC int SDL_SetColors(SDL_Surface *surface, SDL_Color *colors, int firstcolor, int ncolors);
extern DECLSPEC int SDL_SetPalette(SDL_Surface *surface, int flags, SDL_Color *colors, int firstcolor, int ncolors);
extern DECLSPEC Uint32 SDL_MapRGB(SDL_PixelFormat *format, Uint8 r, Uint8 g, Uint8 b);
extern DECLSPEC Uint32 SDL_MapRGBA(SDL_PixelFormat *format, Uint8 r, Uint8 g, Uint8 b, Uint8 a);
extern DECLSPEC void SDL_GetRGB(Uint32 pixel, SDL_PixelFormat *fmt,	Uint8 *r, Uint8 *g, Uint8 *b);
extern DECLSPEC void SDL_GetRGBA(Uint32 pixel, SDL_PixelFormat *fmt, Uint8 *r, Uint8 *g, Uint8 *b, Uint8 *a);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC SDL_Surface * SDL_CreateRGBSurface(Uint32 flags, int width, int height, int depth, Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask);
extern DECLSPEC SDL_Surface * SDL_CreateRGBSurfaceFrom(void *pixels, int width, int height, int depth, int pitch, Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask);
extern DECLSPEC void SDL_FreeSurface(SDL_Surface *surface);

extern DECLSPEC int SDL_LockSurface(SDL_Surface *surface);
extern DECLSPEC void SDL_UnlockSurface(SDL_Surface *surface);

extern DECLSPEC SDL_Surface * SDL_LoadBMP_RW(SDL_RWops *src, int freesrc);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_LoadBMP(file)	SDL_LoadBMP_RW(SDL_RWFromFile(file, "rb"), 1)
#LISP (progn
	(export 'SDL_LoadBMP)
	(defun SDL_LoadBMP (file)
        (SDL_LoadBMP_RW 
            (SDL_RWFromFile file "rb") 1)))

extern DECLSPEC int SDL_SaveBMP_RW(SDL_Surface *surface, SDL_RWops *dst, int freedst);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_AllocSurface    SDL_CreateRGBSurface
#LISP (progn
	(export 'SDL_AllocSurface)
	(defun SDL_AllocSurface ()
            (SDL_CreateRGBSurface)))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_SaveBMP(surface, file) SDL_SaveBMP_RW(surface, SDL_RWFromFile(file, "wb"), 1)
#LISP (progn
	(export 'SDL_SaveBMP)
	(defun SDL_SaveBMP (surface file)
        (SDL_SaveBMP_RW 
            surface (SDL_RWFromFile file "wb") 1)))

extern DECLSPEC int SDL_SetColorKey(SDL_Surface *surface, Uint32 flag, Uint32 key);
extern DECLSPEC int SDL_SetAlpha(SDL_Surface *surface, Uint32 flag, Uint8 alpha);
extern DECLSPEC SDL_bool SDL_SetClipRect(SDL_Surface *surface, const SDL_Rect *rect);
extern DECLSPEC void SDL_GetClipRect(SDL_Surface *surface, SDL_Rect *rect);
extern DECLSPEC SDL_Surface * SDL_ConvertSurface(SDL_Surface *src, SDL_PixelFormat *fmt, Uint32 flags);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_UpperBlit(SDL_Surface *src, SDL_Rect *srcrect, SDL_Surface *dst, SDL_Rect *dstrect);
extern DECLSPEC int SDL_LowerBlit(SDL_Surface *src, SDL_Rect *srcrect, SDL_Surface *dst, SDL_Rect *dstrect);
extern DECLSPEC int SDL_FillRect(SDL_Surface *dst, SDL_Rect *dstrect, Uint32 color);
extern DECLSPEC SDL_Surface * SDL_DisplayFormat(SDL_Surface *surface);
extern DECLSPEC SDL_Surface * SDL_DisplayFormatAlpha(SDL_Surface *surface);

extern DECLSPEC SDL_Overlay * SDL_CreateYUVOverlay(int width, int height, Uint32 format, SDL_Surface *display);
extern DECLSPEC int SDL_LockYUVOverlay(SDL_Overlay *overlay);
extern DECLSPEC void SDL_UnlockYUVOverlay(SDL_Overlay *overlay);
extern DECLSPEC int SDL_DisplayYUVOverlay(SDL_Overlay *overlay, SDL_Rect *dstrect);
extern DECLSPEC void SDL_FreeYUVOverlay(SDL_Overlay *overlay);

extern DECLSPEC int SDL_GL_LoadLibrary(const char *path);
extern DECLSPEC void * SDL_GL_GetProcAddress(const char* proc);
extern DECLSPEC int SDL_GL_SetAttribute(SDL_GLattr attr, int value);
extern DECLSPEC int SDL_GL_GetAttribute(SDL_GLattr attr, int* value);
extern DECLSPEC void SDL_GL_SwapBuffers();
extern DECLSPEC void SDL_GL_UpdateRects(int numrects, SDL_Rect* rects);
extern DECLSPEC void SDL_GL_Lock();
extern DECLSPEC void SDL_GL_Unlock();

extern DECLSPEC void SDL_WM_SetCaption(const char *title, const char *icon);
extern DECLSPEC void SDL_WM_GetCaption(char **title, char **icon);
extern DECLSPEC void SDL_WM_SetIcon(SDL_Surface *icon, Uint8 *mask);
extern DECLSPEC int SDL_WM_IconifyWindow();
extern DECLSPEC int SDL_WM_ToggleFullScreen(SDL_Surface *surface);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_BlitSurface SDL_UpperBlit
#LISP (progn
	(export 'SDL_BlitSurface)
	(defun SDL_BlitSurface (src srcrect dst dstrect)
            (SDL_UpperBlit src srcrect dst dstrect))) 
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef enum {
	SDL_GRAB_QUERY = -1,
	SDL_GRAB_OFF = 0,
	SDL_GRAB_ON = 1,
	SDL_GRAB_FULLSCREEN	/* Used internally */
} SDL_GrabMode;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC SDL_GrabMode SDL_WM_GrabInput(SDL_GrabMode mode);
!#

#|
#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* Not in public API at the moment - do not use! */
extern DECLSPEC int SDLCALL SDL_SoftStretch(SDL_Surface *src, SDL_Rect *srcrect,
                                   SDL_Surface *dst, SDL_Rect *dstrect);
!#
|#

;; 	"SDL_active.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* The available application states */
#define SDL_APPMOUSEFOCUS	0x01		/* The app has mouse coverage */
#define SDL_APPINPUTFOCUS	0x02		/* The app has input focus */
#define SDL_APPACTIVE		0x04		/* The application is active */
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC Uint8 SDL_GetAppState();
!#

;; 	"SDL_keysym.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* What we really want is a mapping of every raw key on the keyboard.
   To support international keyboards, we use the range 0xA1 - 0xFF
   as international virtual keycodes.  We'll follow in the footsteps of X11...
   The names of the keys
 */
 
typedef enum {
	/* The keyboard syms have been cleverly chosen to map to ASCII */
	SDLK_UNKNOWN		= 0,
	SDLK_FIRST		= 0,
	SDLK_BACKSPACE		= 8,
	SDLK_TAB		= 9,
	SDLK_CLEAR		= 12,
	SDLK_RETURN		= 13,
	SDLK_PAUSE		= 19,
	SDLK_ESCAPE		= 27,
	SDLK_SPACE		= 32,
	SDLK_EXCLAIM		= 33,
	SDLK_QUOTEDBL		= 34,
	SDLK_HASH		= 35,
	SDLK_DOLLAR		= 36,
	SDLK_AMPERSAND		= 38,
	SDLK_QUOTE		= 39,
	SDLK_LEFTPAREN		= 40,
	SDLK_RIGHTPAREN		= 41,
	SDLK_ASTERISK		= 42,
	SDLK_PLUS		= 43,
	SDLK_COMMA		= 44,
	SDLK_MINUS		= 45,
	SDLK_PERIOD		= 46,
	SDLK_SLASH		= 47,
	SDLK_0			= 48,
	SDLK_1			= 49,
	SDLK_2			= 50,
	SDLK_3			= 51,
	SDLK_4			= 52,
	SDLK_5			= 53,
	SDLK_6			= 54,
	SDLK_7			= 55,
	SDLK_8			= 56,
	SDLK_9			= 57,
	SDLK_COLON		= 58,
	SDLK_SEMICOLON		= 59,
	SDLK_LESS		= 60,
	SDLK_EQUALS		= 61,
	SDLK_GREATER		= 62,
	SDLK_QUESTION		= 63,
	SDLK_AT			= 64,
	/* 
	   Skip uppercase letters
	 */
	SDLK_LEFTBRACKET	= 91,
	SDLK_BACKSLASH		= 92,
	SDLK_RIGHTBRACKET	= 93,
	SDLK_CARET		= 94,
	SDLK_UNDERSCORE		= 95,
	SDLK_BACKQUOTE		= 96,
	SDLK_a			= 97,
	SDLK_b			= 98,
	SDLK_c			= 99,
	SDLK_d			= 100,
	SDLK_e			= 101,
	SDLK_f			= 102,
	SDLK_g			= 103,
	SDLK_h			= 104,
	SDLK_i			= 105,
	SDLK_j			= 106,
	SDLK_k			= 107,
	SDLK_l			= 108,
	SDLK_m			= 109,
	SDLK_n			= 110,
	SDLK_o			= 111,
	SDLK_p			= 112,
	SDLK_q			= 113,
	SDLK_r			= 114,
	SDLK_s			= 115,
	SDLK_t			= 116,
	SDLK_u			= 117,
	SDLK_v			= 118,
	SDLK_w			= 119,
	SDLK_x			= 120,
	SDLK_y			= 121,
	SDLK_z			= 122,
	SDLK_DELETE		= 127,
	/* End of ASCII mapped keysyms */

	/* International keyboard syms */
	SDLK_WORLD_0		= 160,		/* 0xA0 */
	SDLK_WORLD_1		= 161,
	SDLK_WORLD_2		= 162,
	SDLK_WORLD_3		= 163,
	SDLK_WORLD_4		= 164,
	SDLK_WORLD_5		= 165,
	SDLK_WORLD_6		= 166,
	SDLK_WORLD_7		= 167,
	SDLK_WORLD_8		= 168,
	SDLK_WORLD_9		= 169,
	SDLK_WORLD_10		= 170,
	SDLK_WORLD_11		= 171,
	SDLK_WORLD_12		= 172,
	SDLK_WORLD_13		= 173,
	SDLK_WORLD_14		= 174,
	SDLK_WORLD_15		= 175,
	SDLK_WORLD_16		= 176,
	SDLK_WORLD_17		= 177,
	SDLK_WORLD_18		= 178,
	SDLK_WORLD_19		= 179,
	SDLK_WORLD_20		= 180,
	SDLK_WORLD_21		= 181,
	SDLK_WORLD_22		= 182,
	SDLK_WORLD_23		= 183,
	SDLK_WORLD_24		= 184,
	SDLK_WORLD_25		= 185,
	SDLK_WORLD_26		= 186,
	SDLK_WORLD_27		= 187,
	SDLK_WORLD_28		= 188,
	SDLK_WORLD_29		= 189,
	SDLK_WORLD_30		= 190,
	SDLK_WORLD_31		= 191,
	SDLK_WORLD_32		= 192,
	SDLK_WORLD_33		= 193,
	SDLK_WORLD_34		= 194,
	SDLK_WORLD_35		= 195,
	SDLK_WORLD_36		= 196,
	SDLK_WORLD_37		= 197,
	SDLK_WORLD_38		= 198,
	SDLK_WORLD_39		= 199,
	SDLK_WORLD_40		= 200,
	SDLK_WORLD_41		= 201,
	SDLK_WORLD_42		= 202,
	SDLK_WORLD_43		= 203,
	SDLK_WORLD_44		= 204,
	SDLK_WORLD_45		= 205,
	SDLK_WORLD_46		= 206,
	SDLK_WORLD_47		= 207,
	SDLK_WORLD_48		= 208,
	SDLK_WORLD_49		= 209,
	SDLK_WORLD_50		= 210,
	SDLK_WORLD_51		= 211,
	SDLK_WORLD_52		= 212,
	SDLK_WORLD_53		= 213,
	SDLK_WORLD_54		= 214,
	SDLK_WORLD_55		= 215,
	SDLK_WORLD_56		= 216,
	SDLK_WORLD_57		= 217,
	SDLK_WORLD_58		= 218,
	SDLK_WORLD_59		= 219,
	SDLK_WORLD_60		= 220,
	SDLK_WORLD_61		= 221,
	SDLK_WORLD_62		= 222,
	SDLK_WORLD_63		= 223,
	SDLK_WORLD_64		= 224,
	SDLK_WORLD_65		= 225,
	SDLK_WORLD_66		= 226,
	SDLK_WORLD_67		= 227,
	SDLK_WORLD_68		= 228,
	SDLK_WORLD_69		= 229,
	SDLK_WORLD_70		= 230,
	SDLK_WORLD_71		= 231,
	SDLK_WORLD_72		= 232,
	SDLK_WORLD_73		= 233,
	SDLK_WORLD_74		= 234,
	SDLK_WORLD_75		= 235,
	SDLK_WORLD_76		= 236,
	SDLK_WORLD_77		= 237,
	SDLK_WORLD_78		= 238,
	SDLK_WORLD_79		= 239,
	SDLK_WORLD_80		= 240,
	SDLK_WORLD_81		= 241,
	SDLK_WORLD_82		= 242,
	SDLK_WORLD_83		= 243,
	SDLK_WORLD_84		= 244,
	SDLK_WORLD_85		= 245,
	SDLK_WORLD_86		= 246,
	SDLK_WORLD_87		= 247,
	SDLK_WORLD_88		= 248,
	SDLK_WORLD_89		= 249,
	SDLK_WORLD_90		= 250,
	SDLK_WORLD_91		= 251,
	SDLK_WORLD_92		= 252,
	SDLK_WORLD_93		= 253,
	SDLK_WORLD_94		= 254,
	SDLK_WORLD_95		= 255,		/* 0xFF */

	/* Numeric keypad */
	SDLK_KP0		= 256,
	SDLK_KP1		= 257,
	SDLK_KP2		= 258,
	SDLK_KP3		= 259,
	SDLK_KP4		= 260,
	SDLK_KP5		= 261,
	SDLK_KP6		= 262,
	SDLK_KP7		= 263,
	SDLK_KP8		= 264,
	SDLK_KP9		= 265,
	SDLK_KP_PERIOD		= 266,
	SDLK_KP_DIVIDE		= 267,
	SDLK_KP_MULTIPLY	= 268,
	SDLK_KP_MINUS		= 269,
	SDLK_KP_PLUS		= 270,
	SDLK_KP_ENTER		= 271,
	SDLK_KP_EQUALS		= 272,

	/* Arrows + Home/End pad */
	SDLK_UP			= 273,
	SDLK_DOWN		= 274,
	SDLK_RIGHT		= 275,
	SDLK_LEFT		= 276,
	SDLK_INSERT		= 277,
	SDLK_HOME		= 278,
	SDLK_END		= 279,
	SDLK_PAGEUP		= 280,
	SDLK_PAGEDOWN		= 281,

	/* Function keys */
	SDLK_F1			= 282,
	SDLK_F2			= 283,
	SDLK_F3			= 284,
	SDLK_F4			= 285,
	SDLK_F5			= 286,
	SDLK_F6			= 287,
	SDLK_F7			= 288,
	SDLK_F8			= 289,
	SDLK_F9			= 290,
	SDLK_F10		= 291,
	SDLK_F11		= 292,
	SDLK_F12		= 293,
	SDLK_F13		= 294,
	SDLK_F14		= 295,
	SDLK_F15		= 296,

	/* Key state modifier keys */
	SDLK_NUMLOCK		= 300,
	SDLK_CAPSLOCK		= 301,
	SDLK_SCROLLOCK		= 302,
	SDLK_RSHIFT		= 303,
	SDLK_LSHIFT		= 304,
	SDLK_RCTRL		= 305,
	SDLK_LCTRL		= 306,
	SDLK_RALT		= 307,
	SDLK_LALT		= 308,
	SDLK_RMETA		= 309,
	SDLK_LMETA		= 310,
	SDLK_LSUPER		= 311,		/* Left "Windows" key */
	SDLK_RSUPER		= 312,		/* Right "Windows" key */
	SDLK_MODE		= 313,		/* "Alt Gr" key */
	SDLK_COMPOSE		= 314,		/* Multi-key compose key */

	/* Miscellaneous function keys */
	SDLK_HELP		= 315,
	SDLK_PRINT		= 316,
	SDLK_SYSREQ		= 317,
	SDLK_BREAK		= 318,
	SDLK_MENU		= 319,
	SDLK_POWER		= 320,		/* Power Macintosh power key */
	SDLK_EURO		= 321,		/* Some european keyboards */
	SDLK_UNDO		= 322,		/* Atari keyboard has Undo */

	/* Add any other keys here */

	SDLK_LAST
} SDLKey;

/* Enumeration of valid key mods (possibly OR'd together) */
typedef enum {
	KMOD_NONE  = 0x0000,
	KMOD_LSHIFT= 0x0001,
	KMOD_RSHIFT= 0x0002,
	KMOD_LCTRL = 0x0040,
	KMOD_RCTRL = 0x0080,
	KMOD_LALT  = 0x0100,
	KMOD_RALT  = 0x0200,
	KMOD_LMETA = 0x0400,
	KMOD_RMETA = 0x0800,
	KMOD_NUM   = 0x1000,
	KMOD_CAPS  = 0x2000,
	KMOD_MODE  = 0x4000,
	KMOD_RESERVED = 0x8000
} SDLMod;

#define KMOD_CTRL	(KMOD_LCTRL|KMOD_RCTRL)
#define KMOD_SHIFT	(KMOD_LSHIFT|KMOD_RSHIFT)
#define KMOD_ALT	(KMOD_LALT|KMOD_RALT)
#define KMOD_META	(KMOD_LMETA|KMOD_RMETA)

!#

;; 	"SDL_keyboard.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef struct {
	Uint8 scancode;			/* hardware specific scancode */
	SDLKey sym;			/* SDL virtual keysym */
	SDLMod mod;			/* current key modifiers */
	Uint16 unicode;			/* translated character */
} SDL_keysym;

/* This is the mask which refers to all hotkey bindings */
#define SDL_ALL_HOTKEYS		0xFFFFFFFF
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_EnableUNICODE(int enable);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_DEFAULT_REPEAT_DELAY	500
#define SDL_DEFAULT_REPEAT_INTERVAL	30
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_EnableKeyRepeat(int delay, int interval);
extern DECLSPEC Uint8 * SDL_GetKeyState(int *numkeys);
extern DECLSPEC SDLMod SDL_GetModState();
extern DECLSPEC void SDL_SetModState(SDLMod modstate);
extern DECLSPEC char * SDL_GetKeyName(SDLKey key);
!#

;; 	"SDL_mouse.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
struct WMcursor;
typedef struct WMcursor WMcursor;	/* Implementation dependent */
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef struct {
	SDL_Rect area;			/* The area of the mouse cursor */
	Sint16 hot_x;
	Sint16 hot_y;		/* The "tip" of the cursor */
	Uint8 *data;			/* B/W cursor data */
	Uint8 *mask;			/* B/W cursor mask */
	Uint8 *save[2];			/* Place to save cursor area */
	WMcursor *wm_cursor;		/* Window-manager cursor */
} SDL_Cursor;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC Uint8 SDL_GetMouseState(int *x, int *y);
extern DECLSPEC Uint8 SDL_GetRelativeMouseState(int *x, int *y);
extern DECLSPEC void SDL_WarpMouse(Uint16 x, Uint16 y);
extern DECLSPEC SDL_Cursor * SDL_CreateCursor(Uint8 *data, Uint8 *mask, int w, int h, int hot_x, int hot_y);
extern DECLSPEC void SDL_SetCursor(SDL_Cursor *cursor);
extern DECLSPEC SDL_Cursor * SDL_GetCursor();
extern DECLSPEC void SDL_FreeCursor(SDL_Cursor *cursor);
extern DECLSPEC int SDL_ShowCursor(int toggle);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_BUTTON(X)		(SDL_PRESSED<<(X-1))
#LISP (progn
	(export 'SDL_BUTTON)
	(defun SDL_BUTTON (X)
		(ash SDL_PRESSED (- X 1))))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_BUTTON_LEFT		1
#define SDL_BUTTON_MIDDLE	2
#define SDL_BUTTON_RIGHT	3
#define SDL_BUTTON_WHEELUP	4
#define SDL_BUTTON_WHEELDOWN	5

//#define SDL_BUTTON_LMASK	SDL_BUTTON(SDL_BUTTON_LEFT)
#LISP (progn
	(export 'SDL_BUTTON_LMASK)
	(defun SDL_BUTTON_LMASK ()
		(SDL_BUTTON SDL_BUTTON_LEFT)))

//#define SDL_BUTTON_MMASK	SDL_BUTTON(SDL_BUTTON_MIDDLE)
#LISP (progn
	(export 'SDL_BUTTON_MMASK)
	(defun SDL_BUTTON_MMASK ()
		(SDL_BUTTON SDL_BUTTON_MIDDLE)))

//#define SDL_BUTTON_RMASK	SDL_BUTTON(SDL_BUTTON_RIGHT)
#LISP (progn
	(export 'SDL_BUTTON_RMASK)
	(defun SDL_BUTTON_RMASK ()
		(SDL_BUTTON SDL_BUTTON_RIGHT)))
!#

;; 	"SDL_joystick.h"
;; 

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
struct _SDL_Joystick;
typedef struct _SDL_Joystick SDL_Joystick;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_NumJoysticks();
extern DECLSPEC const char * SDL_JoystickName(int device_index);
extern DECLSPEC SDL_Joystick * SDL_JoystickOpen(int device_index);
extern DECLSPEC int SDL_JoystickOpened(int device_index);
extern DECLSPEC int SDL_JoystickIndex(SDL_Joystick *joystick);
extern DECLSPEC int SDL_JoystickNumAxes(SDL_Joystick *joystick);
extern DECLSPEC int SDL_JoystickNumBalls(SDL_Joystick *joystick);
extern DECLSPEC int SDL_JoystickNumHats(SDL_Joystick *joystick);
extern DECLSPEC int SDL_JoystickNumButtons(SDL_Joystick *joystick);
extern DECLSPEC void SDL_JoystickUpdate();
extern DECLSPEC int SDL_JoystickEventState(int state);
extern DECLSPEC Sint16 SDL_JoystickGetAxis(SDL_Joystick *joystick, int axis);
!#

#! (:library "SDL" :export t :ignore "DECLSPEC" )
#define SDL_HAT_CENTERED	0x00
#define SDL_HAT_UP		    0x01
#define SDL_HAT_RIGHT		0x02
#define SDL_HAT_DOWN		0x04
#define SDL_HAT_LEFT		0x08
#define SDL_HAT_RIGHTUP		(SDL_HAT_RIGHT | SDL_HAT_UP)
#define SDL_HAT_RIGHTDOWN	(SDL_HAT_RIGHT | SDL_HAT_DOWN)
#define SDL_HAT_LEFTUP		(SDL_HAT_LEFT | SDL_HAT_UP)
#define SDL_HAT_LEFTDOWN	(SDL_HAT_LEFT | SDL_HAT_DOWN)
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC Uint8 SDL_JoystickGetHat(SDL_Joystick *joystick, int hat);
extern DECLSPEC int SDL_JoystickGetBall(SDL_Joystick *joystick, int ball, int *dx, int *dy);
extern DECLSPEC Uint8 SDL_JoystickGetButton(SDL_Joystick *joystick, int button);
extern DECLSPEC void SDL_JoystickClose(SDL_Joystick *joystick);
!#

;;	"SDL_events.h"
;;

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* Event enumerations */
enum { SDL_NOEVENT = 0,			/* Unused (do not remove) */
       SDL_ACTIVEEVENT,			/* Application loses/gains visibility */
       SDL_KEYDOWN,			/* Keys pressed */
       SDL_KEYUP,			/* Keys released */
       SDL_MOUSEMOTION,			/* Mouse moved */
       SDL_MOUSEBUTTONDOWN,		/* Mouse button pressed */
       SDL_MOUSEBUTTONUP,		/* Mouse button released */
       SDL_JOYAXISMOTION,		/* Joystick axis motion */
       SDL_JOYBALLMOTION,		/* Joystick trackball motion */
       SDL_JOYHATMOTION,		/* Joystick hat position change */
       SDL_JOYBUTTONDOWN,		/* Joystick button pressed */
       SDL_JOYBUTTONUP,			/* Joystick button released */
       SDL_QUIT,			/* User-requested quit */
       SDL_SYSWMEVENT,			/* System specific event */
       SDL_EVENT_RESERVEDA,		/* Reserved for future use.. */
       SDL_EVENT_RESERVEDB,		/* Reserved for future use.. */
       SDL_VIDEORESIZE,			/* User resized video mode */
       SDL_VIDEOEXPOSE,			/* Screen needs to be redrawn */
       SDL_EVENT_RESERVED2,		/* Reserved for future use.. */
       SDL_EVENT_RESERVED3,		/* Reserved for future use.. */
       SDL_EVENT_RESERVED4,		/* Reserved for future use.. */
       SDL_EVENT_RESERVED5,		/* Reserved for future use.. */
       SDL_EVENT_RESERVED6,		/* Reserved for future use.. */
       SDL_EVENT_RESERVED7,		/* Reserved for future use.. */
       /* Events SDL_USEREVENT through SDL_MAXEVENTS-1 are for your use */
       SDL_USEREVENT = 24,
       /* This last event is only for bounding internal arrays
	  It is the number of bits in the event mask datatype -- Uint32
        */
       SDL_NUMEVENTS = 32
};
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* Predefined event masks */
//#define SDL_EVENTMASK(X)	(1<<(X))
#LISP (progn
	(export 'SDL_EVENTMASK)
	(defun SDL_EVENTMASK (X)
		(ash 1 X )))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//SDL_ACTIVEEVENTMASK	= SDL_EVENTMASK(SDL_ACTIVEEVENT)
#LISP (progn
	(export 'SDL_ACTIVEEVENTMASK)
	(defun SDL_ACTIVEEVENTMASK ()
		(SDL_EVENTMASK SDL_ACTIVEEVENT)))

//SDL_KEYDOWNMASK		= SDL_EVENTMASK(SDL_KEYDOWN),
#LISP (progn
	(export 'SDL_KEYDOWNMASK)
	(defun SDL_KEYDOWNMASK ()
		(SDL_EVENTMASK SDL_KEYDOWN)))

//SDL_KEYUPMASK		= SDL_EVENTMASK(SDL_KEYUP),
#LISP (progn
	(export 'SDL_KEYUPMASK)
	(defun SDL_KEYUPMASK ()
		(SDL_EVENTMASK SDL_KEYUP)))

//SDL_MOUSEMOTIONMASK	= SDL_EVENTMASK(SDL_MOUSEMOTION),
#LISP (progn
	(export 'SDL_MOUSEMOTIONMASK)
	(defun SDL_MOUSEMOTIONMASK ()
		(SDL_EVENTMASK SDL_MOUSEMOTION)))

//SDL_MOUSEBUTTONDOWNMASK	= SDL_EVENTMASK(SDL_MOUSEBUTTONDOWN),
#LISP (progn
	(export 'SDL_MOUSEBUTTONDOWNMASK)
	(defun SDL_MOUSEBUTTONDOWNMASK ()
		(SDL_EVENTMASK SDL_MOUSEBUTTONDOWN)))

//SDL_MOUSEBUTTONUPMASK	= SDL_EVENTMASK(SDL_MOUSEBUTTONUP),
#LISP (progn
	(export 'SDL_MOUSEBUTTONUPMASK)
	(defun SDL_MOUSEBUTTONUPMASK ()
		(SDL_EVENTMASK SDL_MOUSEBUTTONUP)))

//SDL_MOUSEEVENTMASK	= SDL_EVENTMASK(SDL_MOUSEMOTION)|
//	                          SDL_EVENTMASK(SDL_MOUSEBUTTONDOWN)|
//	                          SDL_EVENTMASK(SDL_MOUSEBUTTONUP),
#LISP (progn
	(export 'SDL_MOUSEEVENTMASK)
	(defun SDL_MOUSEEVENTMASK ()
	(logior (SDL_EVENTMASK SDL_MOUSEMOTION)
			(SDL_EVENTMASK SDL_MOUSEBUTTONDOWN)
			(SDL_EVENTMASK SDL_MOUSEBUTTONUP))))

//SDL_JOYAXISMOTIONMASK	= SDL_EVENTMASK(SDL_JOYAXISMOTION),
#LISP (progn
	(export 'SDL_JOYAXISMOTIONMASK)
	(defun SDL_JOYAXISMOTIONMASK ()
		(SDL_EVENTMASK SDL_JOYAXISMOTION)))

//SDL_JOYBALLMOTIONMASK	= SDL_EVENTMASK(SDL_JOYBALLMOTION),
#LISP (progn
	(export 'SDL_JOYBALLMOTIONMASK)
	(defun SDL_JOYBALLMOTIONMASK ()
		(SDL_EVENTMASK SDL_JOYBALLMOTION)))

//SDL_JOYHATMOTIONMASK	= SDL_EVENTMASK(SDL_JOYHATMOTION),
#LISP (progn
	(export 'SDL_JOYHATMOTIONMASK)
	(defun SDL_JOYHATMOTIONMASK ()
		(SDL_EVENTMASK SDL_JOYHATMOTION)))

//SDL_JOYBUTTONDOWNMASK	= SDL_EVENTMASK(SDL_JOYBUTTONDOWN),
#LISP (progn
	(export 'SDL_JOYBUTTONDOWNMASK)
		(defun SDL_JOYBUTTONDOWNMASK ()
		(SDL_EVENTMASK SDL_JOYBUTTONDOWN)))

//SDL_JOYBUTTONUPMASK	= SDL_EVENTMASK(SDL_JOYBUTTONUP),
#LISP (progn
	(export 'SDL_JOYBUTTONUPMASK)
	(defun SDL_JOYBUTTONUPMASK ()
		(SDL_EVENTMASK SDL_JOYBUTTONUP)))

//SDL_JOYEVENTMASK	= SDL_EVENTMASK(SDL_JOYAXISMOTION)|
//                          SDL_EVENTMASK(SDL_JOYBALLMOTION)|
//                          SDL_EVENTMASK(SDL_JOYHATMOTION)|
//                          SDL_EVENTMASK(SDL_JOYBUTTONDOWN)|
//                          SDL_EVENTMASK(SDL_JOYBUTTONUP),
#LISP (progn
	(export 'SDL_JOYEVENTMASK)
	(defun SDL_JOYEVENTMASK ()
		(logior (SDL_EVENTMASK SDL_JOYAXISMOTION)
			(SDL_EVENTMASK SDL_JOYBALLMOTION)
			(SDL_EVENTMASK SDL_JOYHATMOTION)
			(SDL_EVENTMASK SDL_JOYBUTTONDOWN)
			(SDL_EVENTMASK SDL_JOYBUTTONUP))))

//SDL_VIDEORESIZEMASK	= SDL_EVENTMASK(SDL_VIDEORESIZE),
#LISP (progn
	(export 'SDL_VIDEORESIZEMASK)
	(defun SDL_VIDEORESIZEMASK () 
		(SDL_EVENTMASK SDL_VIDEORESIZE)))

//SDL_VIDEOEXPOSEMASK	= SDL_EVENTMASK(SDL_VIDEOEXPOSE),
#LISP (progn
	(export 'SDL_VIDEOEXPOSEMASK)
	(defun SDL_VIDEOEXPOSEMASK ()
		(SDL_EVENTMASK SDL_VIDEOEXPOSE)))

//SDL_QUITMASK		= SDL_EVENTMASK(SDL_QUIT),
#LISP (progn
	(export 'SDL_QUITMASK)
	(defun SDL_QUITMASK ()
			(SDL_EVENTMASK SDL_QUIT)))

//SDL_SYSWMEVENTMASK	= SDL_EVENTMASK(SDL_SYSWMEVENT)
#LISP (progn
	(export 'SDL_SYSWMEVENTMASK)
	(defun SDL_SYSWMEVENTMASK ()
		(SDL_EVENTMASK SDL_SYSWMEVENT)))

#define SDL_ALLEVENTS		0xFFFFFFFF

/* Application visibility event structure */
typedef struct {
	Uint8 type;	/* SDL_ACTIVEEVENT */
	Uint8 gain;	/* Whether given states were gained or lost (1/0) */
	Uint8 state;	/* A mask of the focus states */
} SDL_ActiveEvent;

/* Keyboard event structure */
typedef struct {
	Uint8 type;	/* SDL_KEYDOWN or SDL_KEYUP */
	Uint8 which;	/* The keyboard device index */
	Uint8 state;	/* SDL_PRESSED or SDL_RELEASED */
	SDL_keysym keysym;
} SDL_KeyboardEvent;

/* Mouse motion event structure */
typedef struct {
	Uint8 type;	/* SDL_MOUSEMOTION */
	Uint8 which;	/* The mouse device index */
	Uint8 state;	/* The current button state */
	Uint16 x;
	Uint16 y;	/* The X/Y coordinates of the mouse */
	Sint16 xrel;	/* The relative motion in the X direction */
	Sint16 yrel;	/* The relative motion in the Y direction */
} SDL_MouseMotionEvent;

/* Mouse button event structure */
typedef struct {
	Uint8 type;	/* SDL_MOUSEBUTTONDOWN or SDL_MOUSEBUTTONUP */
	Uint8 which;	/* The mouse device index */
	Uint8 button;	/* The mouse button index */
	Uint8 state;	/* SDL_PRESSED or SDL_RELEASED */
	Uint16 x;
	Uint16 y;	/* The X/Y coordinates of the mouse at press time */
} SDL_MouseButtonEvent;

/* Joystick axis motion event structure */
typedef struct {
	Uint8 type;	/* SDL_JOYAXISMOTION */
	Uint8 which;	/* The joystick device index */
	Uint8 axis;	/* The joystick axis index */
	Sint16 value;	/* The axis value (range: -32768 to 32767) */
} SDL_JoyAxisEvent;

/* Joystick trackball motion event structure */
typedef struct {
	Uint8 type;	/* SDL_JOYBALLMOTION */
	Uint8 which;	/* The joystick device index */
	Uint8 ball;	/* The joystick trackball index */
	Sint16 xrel;	/* The relative motion in the X direction */
	Sint16 yrel;	/* The relative motion in the Y direction */
} SDL_JoyBallEvent;

/* Joystick hat position change event structure */
typedef struct {
	Uint8 type;	/* SDL_JOYHATMOTION */
	Uint8 which;	/* The joystick device index */
	Uint8 hat;	/* The joystick hat index */
	Uint8 value;	/* The hat position value:
			    SDL_HAT_LEFTUP   SDL_HAT_UP       SDL_HAT_RIGHTUP
			    SDL_HAT_LEFT     SDL_HAT_CENTERED SDL_HAT_RIGHT
			    SDL_HAT_LEFTDOWN SDL_HAT_DOWN     SDL_HAT_RIGHTDOWN
			   Note that zero means the POV is centered.
			*/
} SDL_JoyHatEvent;

/* Joystick button event structure */
typedef struct {
	Uint8 type;	/* SDL_JOYBUTTONDOWN or SDL_JOYBUTTONUP */
	Uint8 which;	/* The joystick device index */
	Uint8 button;	/* The joystick button index */
	Uint8 state;	/* SDL_PRESSED or SDL_RELEASED */
} SDL_JoyButtonEvent;

/* The "window resized" event
   When you get this event, you are responsible for setting a new video
   mode with the new width and height.
 */
typedef struct {
	Uint8 type;	/* SDL_VIDEORESIZE */
	int w;		/* New width */
	int h;		/* New height */
} SDL_ResizeEvent;

/* The "screen redraw" event */
typedef struct {
	Uint8 type;	/* SDL_VIDEOEXPOSE */
} SDL_ExposeEvent;

/* The "quit requested" event */
typedef struct {
	Uint8 type;	/* SDL_QUIT */
} SDL_QuitEvent;

/* A user-defined event type */
typedef struct {
	Uint8 type;	/* SDL_USEREVENT through SDL_NUMEVENTS-1 */
	int code;	/* User defined event code */
	void *data1;	/* User defined data pointer */
	void *data2;	/* User defined data pointer */
} SDL_UserEvent;

/* If you want to use this event, you should include SDL_syswm.h */
struct SDL_SysWMmsg;
typedef struct SDL_SysWMmsg SDL_SysWMmsg;
typedef struct {
	Uint8 type;
	SDL_SysWMmsg *msg;
} SDL_SysWMEvent;
!#

; The C to Lisp translator does not support unions. And as I am too 
; lazy to determine the exact largest size for the SDL_Event struct,
; we shall create a struct that we hope is large enough to hold any event.
; If something unexplained happens during runtime, e.g. weird crashes etc.
; then take a look at this struct, it may be causing the problem.
; Note: variables within the struct are aligned on int boundaries. The 
; C->Lisp parser should take care of this automatically. If something 
; doesn't seem to be working the way it is supposed to, check these as well.

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/*
typedef union {
	Uint8 type;
	SDL_ActiveEvent active;
	SDL_KeyboardEvent key;
	SDL_MouseMotionEvent motion;
	SDL_MouseButtonEvent button;
	SDL_JoyAxisEvent jaxis;
	SDL_JoyBallEvent jball;
	SDL_JoyHatEvent jhat;
	SDL_JoyButtonEvent jbutton;
	SDL_ResizeEvent resize;
	SDL_ExposeEvent expose;
	SDL_QuitEvent quit;
	SDL_UserEvent user;
	SDL_SysWMEvent syswm;
} SDL_Event;
*/

typedef struct {
	Uint8 type;
    Uint8 buffer[1023];
} SDL_Event;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC void SDL_PumpEvents();
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef enum {
	SDL_ADDEVENT,
	SDL_PEEKEVENT,
	SDL_GETEVENT
} SDL_eventaction;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_PeepEvents(SDL_Event *events, int numevents, SDL_eventaction action, Uint32 mask);
extern DECLSPEC int SDL_PollEvent(SDL_Event *event);
extern DECLSPEC int SDL_WaitEvent(SDL_Event *event);
extern DECLSPEC int SDL_PushEvent(SDL_Event *event);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef int (*SDL_EventFilter)(const SDL_Event *event);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC void SDL_SetEventFilter(SDL_EventFilter filter);
extern DECLSPEC SDL_EventFilter SDL_GetEventFilter();
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_QUERY	-1
#define SDL_IGNORE	 0
#define SDL_DISABLE	 0
#define SDL_ENABLE	 1
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC Uint8 SDL_EventState(Uint8 type, int state);
!#

;;	"SDL_quit.h"
;;
#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_QuitRequested() \
//        (SDL_PumpEvents(), SDL_PeepEvents(NULL,0,SDL_PEEKEVENT,SDL_QUITMASK))
!#


;;	"SDL_timer.h"
;;

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_TIMESLICE		10
#define TIMER_RESOLUTION	10	/* Experimentally determined */
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC Uint32  SDL_GetTicks();
extern DECLSPEC void  SDL_Delay(Uint32 ms);

/* Function prototype for the timer callback function */
typedef Uint32 (*SDL_TimerCallback)(Uint32 interval);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_SetTimer(Uint32 interval, SDL_TimerCallback callback);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef Uint32 (*SDL_NewTimerCallback)(Uint32 interval, void *param);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )

//typedef struct _SDL_TimerID *SDL_TimerID;
struct _SDL_TimerID;
typedef struct _SDL_TimerID *SDL_TimerID;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC SDL_TimerID SDL_AddTimer(Uint32 interval, SDL_NewTimerCallback callback, void *param);
extern DECLSPEC SDL_bool  SDL_RemoveTimer(SDL_TimerID);
!#


;;	"SDL_getenv.h"
;;

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* Put a variable of the form "name=value" into the environment */
extern DECLSPEC int SDL_putenv(const char *variable);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define putenv(X)   SDL_putenv(X)
#LISP (progn
	(export 'putenv)
	(defun putenv (x)
            (SDL_putenv x)))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* Retrieve a variable named "name" from the environment */
extern DECLSPEC char * SDL_getenv(const char *name);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define getenv(X)     SDL_getenv(X)
#LISP (progn
	(export 'getenv)
	(defun getenv (x)
            (SDL_getenv x)))
!#

;;	"SDL_byteorder.h"
;;

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_LIL_ENDIAN	1234
#define SDL_BIG_ENDIAN	4321
!#
#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_BYTEORDER	SDL_BIG_ENDIAN
!#

;;	"SDL_endian.h"
;;



;;	"SDL_cdrom.h"
;;

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_MAX_TRACKS	99
#define SDL_AUDIO_TRACK	0x00
#define SDL_DATA_TRACK	0x04

typedef enum {
	CD_TRAYEMPTY,
	CD_STOPPED,
	CD_PLAYING,
	CD_PAUSED,
	CD_ERROR = -1
} CDstatus;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define CD_INDRIVE(status)	((int)status > 0)
#LISP (progn
	(export 'CD_INDRIVE)
	(defun CD_INDRIVE (status)
		(if (> status 0)
            t
            nil)))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef struct {
	Uint8 id;		/* Track number */
	Uint8 type;		/* Data or audio track */
	Uint16 unused;
	Uint32 length;		/* Length, in frames, of this track */
	Uint32 offset;		/* Offset, in frames, from start of disk */
} SDL_CDtrack;

typedef struct SDL_CD {
	int id;			/* Private drive identifier */
	CDstatus status;	/* Current drive status */

	/* The rest of this structure is only valid if there's a CD in drive */
	int numtracks;		/* Number of tracks on disk */
	int cur_track;		/* Current track position */
	int cur_frame;		/* Current frame offset within current track */
//	SDL_CDtrack track[SDL_MAX_TRACKS+1];
    SDL_CDtrack track[100];
} SDL_CD;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define CD_FPS	75
/*
#define FRAMES_TO_MSF(f, M,S,F)	{					\
	int value = f;							\
	*(F) = value%CD_FPS;						\
	value /= CD_FPS;						\
	*(S) = value%60;						\
	value /= 60;							\
	*(M) = value;							\
}
*/
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#LISP (progn
	(export 'FRAMES_TO_MSF)
	(defun FRAMES_TO_MSF (f)
            (values 
                (mod f CD_FPS)
                (mod (/ f CD_FPS) 60)
                (/ (/ f CD_FPS) 60))))

//#define MSF_TO_FRAMES(M, S, F)	((M)*60*CD_FPS+(S)*CD_FPS+(F))
#LISP (progn
	(export 'MSF_TO_FRAMES)
	(defun MSF_TO_FRAMES (M S F)
        (+ 
            (* M 60 CD_FPS)
            (* S CD_FPS)
            F)))
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_CDNumDrives();
extern DECLSPEC const char * SDL_CDName(int drive);
extern DECLSPEC SDL_CD * SDL_CDOpen(int drive);
extern DECLSPEC CDstatus SDL_CDStatus(SDL_CD *cdrom);
extern DECLSPEC int SDL_CDPlayTracks(SDL_CD *cdrom,
		int start_track, int start_frame, int ntracks, int nframes);
extern DECLSPEC int SDL_CDPlay(SDL_CD *cdrom, int start, int length);
extern DECLSPEC int SDL_CDPause(SDL_CD *cdrom);
extern DECLSPEC int SDL_CDResume(SDL_CD *cdrom);
extern DECLSPEC int SDL_CDStop(SDL_CD *cdrom);
extern DECLSPEC int SDL_CDEject(SDL_CD *cdrom);
extern DECLSPEC void SDL_CDClose(SDL_CD *cdrom);
!#

;;	"SDL_audio.h"
;;

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* The calculated values in this structure are calculated by SDL_OpenAudio() */
typedef struct {
	int freq;		/* DSP frequency -- samples per second */
	Uint16 format;		/* Audio data format */
	Uint8  channels;	/* Number of channels: 1 mono, 2 stereo */
	Uint8  silence;		/* Audio buffer silence value (calculated) */
	Uint16 samples;		/* Audio buffer size in samples (power of 2) */
	Uint16 padding;		/* Necessary for some compile environments */
	Uint32 size;		/* Audio buffer size in bytes (calculated) */
	/* This function is called when the audio device needs more data.
	   'stream' is a pointer to the audio data buffer
	   'len' is the length of that buffer in bytes.
	   Once the callback returns, the buffer will no longer be valid.
	   Stereo samples are stored in a LRLRLR ordering.
	*/
	void (*callback)(void *userdata, Uint8 *stream, int len);
	void  *userdata;
} SDL_AudioSpec;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* Audio format flags (defaults to LSB byte order) */
#define AUDIO_U8	0x0008	/* Unsigned 8-bit samples */
#define AUDIO_S8	0x8008	/* Signed 8-bit samples */
#define AUDIO_U16LSB	0x0010	/* Unsigned 16-bit samples */
#define AUDIO_S16LSB	0x8010	/* Signed 16-bit samples */
#define AUDIO_U16MSB	0x1010	/* As above, but big-endian byte order */
#define AUDIO_S16MSB	0x9010	/* As above, but big-endian byte order */
#define AUDIO_U16	AUDIO_U16LSB
#define AUDIO_S16	AUDIO_S16LSB

#define AUDIO_U16SYS	AUDIO_U16MSB
#define AUDIO_S16SYS	AUDIO_S16MSB
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* A structure to hold a set of audio conversion filters and buffers */
//typedef struct SDL_AudioCVT {
//	int needed;			/* Set to 1 if conversion possible */
//	Uint16 src_format;		/* Source audio format */
//	Uint16 dst_format;		/* Target audio format */
//	double rate_incr;		/* Rate conversion increment */
//	Uint8 *buf;			/* Buffer to hold entire audio data */
//	int    len;			/* Length of original audio buffer */
//	int    len_cvt;			/* Length of converted audio buffer */
//	int    len_mult;		/* buffer must be len*len_mult big */
//	double len_ratio; 	/* Given len, final size is len*len_ratio */
//	void (*filters[10])(struct SDL_AudioCVT *cvt, Uint16 format);
//	int filter_index;		/* Current audio conversion function */
//} SDL_AudioCVT;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC int SDL_AudioInit(const char *driver_name);
extern DECLSPEC void SDL_AudioQuit();
extern DECLSPEC char * SDL_AudioDriverName(char *namebuf, int maxlen);
extern DECLSPEC int SDL_OpenAudio(SDL_AudioSpec *desired, SDL_AudioSpec *obtained);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
typedef enum {
	SDL_AUDIO_STOPPED = 0,
	SDL_AUDIO_PLAYING,
	SDL_AUDIO_PAUSED
} SDL_audiostatus;
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC SDL_audiostatus SDL_GetAudioStatus();
extern DECLSPEC void SDL_PauseAudio(int pause_on);
extern DECLSPEC SDL_AudioSpec * SDL_LoadWAV_RW(SDL_RWops *src, int freesrc, SDL_AudioSpec *spec, Uint8 **audio_buf, Uint32 *audio_len);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
//#define SDL_LoadWAV(file, spec, audio_buf, audio_len) \
//	SDL_LoadWAV_RW(SDL_RWFromFile(file, "rb"),1, spec,audio_buf,audio_len)
#LISP (progn
	(export 'SDL_LoadWAV)
	(defun SDL_LoadWAV (file spec audio_buf audio_len)
        (SDL_LoadWAV_RW 
            (SDL_RWFromFile file "rb")
            1
            spec
            audio_buf
            audio_len)))

extern DECLSPEC void SDL_FreeWAV(Uint8 *audio_buf);
extern DECLSPEC int SDL_BuildAudioCVT(SDL_AudioCVT *cvt,
		Uint16 src_format, Uint8 src_channels, int src_rate,
		Uint16 dst_format, Uint8 dst_channels, int dst_rate);
extern DECLSPEC int SDL_ConvertAudio(SDL_AudioCVT *cvt);
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
#define SDL_MIX_MAXVOLUME 128
!#

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
extern DECLSPEC void SDL_MixAudio(Uint8 *dst, const Uint8 *src, Uint32 len, int volume);
extern DECLSPEC void SDL_LockAudio();
extern DECLSPEC void SDL_UnlockAudio();
extern DECLSPEC void SDL_CloseAudio();
!#

;;	"SDL_loadso.h"
;;

#! (:export t :library "SDL" :ignore "DECLSPEC" :pack 4 )
/* This function dynamically loads a shared object and returns a pointer
 * to the object handle (or NULL if there was an error).
 * The 'sofile' parameter is a system dependent name of the object file.
 */
extern DECLSPEC void * SDL_LoadObject(const char *sofile);

/* Given an object handle, this function looks up the address of the
 * named function in the shared object and returns it.  This address
 * is no longer valid after calling SDL_UnloadObject().
 */
extern DECLSPEC void * SDL_LoadFunction(void *handle, const char *name);

/* Unload a shared object from memory */
extern DECLSPEC void SDL_UnloadObject(void *handle);
!#
    
(provide :sdl)


;;(ccl:set-current-directory "C:/Documents and Settings/00u4440/My Documents/dev/corman-sdl/ffi")
;;(ccl:set-current-directory "F:/Documents and Settings/Crook/My Documents/dev/corman-sdl/corman-sdl/ffi")

;;(require :MEM-C-FUNCS)
;;(ct:transcribe-file "sdl_h.lisp" "sdl.lisp" :common-lisp-user nil)
;;(ct:transcribe-file "sdl_h.lisp" "sdl.lisp" :rm nil)
