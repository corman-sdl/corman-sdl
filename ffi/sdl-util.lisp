;;;;; SDL Utilities for Corman-SDL
;;;;; FFI bindings to SDL (http://www.libsdl.org ) for 
;;;;; Corman Common Lisp ( http://www.cormanlisp.com )
;;;;; Copyright (c) 2003-2004, Luke J Crook
;;;;; All rights reserved.

;;;;; Version 0.1 , 02.24.2004
;;;;; Version 0.2 , 04.02.2004

(require 'sdl)
(in-package :sdl)

(export '(for while aif fformat to-radian to-degree calculate-timescale get-timescale display flip with-locksurface
        DisplayFormat set-flags init-sdl set-videomode new-screen destruct-sdl geterror with-events with-init loadbmp
        get-key new-event new-rect push-quitevent getvideoinfo listmodes videomodeok add-surface get-surface
        calculate-timescale init-success pixelformat fill-display clear-display blit-to-display blit-to-surface
        fill-surface update-display set-colorkey clear-colorkey is-key moveby-rectangle moveto-rectangle
        rectangle-x rectangle-y rectangle-w rectangle-h surface-w surface-h set-rectangle))

(defmacro for (var start stop &body body)
    (let ((gstop (gensym)))
        `(do (
                (,var ,start (1+ ,var))
                (,gstop ,stop))
            ((> ,var ,gstop))
            ,@body)))

(defmacro while (test &rest body)
    `(do ()
        ((not ,test))
        ,@body))

(defmacro aif (test-form then-form &optional else-form)
    "An anamorphic macro, from 'On Lisp' by Paul Graham.
    e.g. (aif (big-long-calculation)
              (foo it))"
   `(let ((it ,test-form))
        (if it ,then-form ,else-form)))

;; Removes need for stream format argument (always to *standard-output*),
;; always puts a newline at the end and forces output immediately, all
;; for ease of debugging.
(defun fformat (&rest args)
    "Removes the need for stream format argument (always to *standard-output*),
    always puts a newline at the end and forces output immediately, 
    all for ease of debugging."
    (apply #'format t args) (terpri t) (force-output t))

(defun to-radian (degree)
    (* degree (/ PI 180)))

(defun to-degree (radian)
    (/ radian (/ PI 180)))

(let ((timescale nil))
    (defun set-timescale (tscale)
        (setf timescale tscale))
    (defun get-timescale ()
        timescale))

(let ((surface nil))
    (defun set-display (surf)
        (setf surface surf))
    (defun display ()
        "Return the display surface: SDL_Surface"
        surface))

(let ((engine nil))
    (defun engine-init ()
        (when (null engine)
            (setf engine (make-instance 'engine)))
        engine)
    (defun get-engine ()
        engine))

(defun flip ()
    "When double buffering, flip will change the current buffer."
    (when (display)
        (sdl:SDL_Flip (display))))

(defmacro with-locksurface (surface &body body)
    "WITH-LOCKSURFACE sets up a surface for directly accessing the pixels using sdl:SDL_LockSurface.
    WITH-LOCKSURFACE uses sdl:SDL_MUSTLOCK to first check if the surface should be locked.
    Within WITH-LOCKSURFACE you can write to and read from surface->pixels, 
    using the pixel format stored in surface->format."
    (let ((surf (gensym)))
        `(let ((,surf ,surface))
            (block nil
                (when (eql 1 (sdl_mustlock ,surf))
                    (if (eql 0 (sdl:SDL_LockSurface ,surf))
                        (progn
                            ,@body)
                        (error "Cannot lock surface")))
                (when (eql 1 (sdl_mustlock ,surf))
                    (sdl:SDL_UnlockSurface ,surf))))))

(defun DisplayFormat (surface &key (alpha nil))
    "displayformat (surface &key (alpha nil))
    Calls sdl:SDL_DisplayFormat, passing surface as the parameter.
    :alpha t calls the function SDL_DisplayFormatAlpha, converting 
    the surface and adding an alpha channel.
    Retuns NIL if sdl:SDL_DisplayFormat fails."
    (if (or 
            (null surface)
            (not (ct:cpointerp surface)))
        nil
        (if alpha
            (let ((surf (sdl:SDL_DisplayFormatAlpha surface)))
                (when (ct:cpointer-null surf)
                    (setf surf nil))
                surf)
            (let ((surf (sdl:SDL_DisplayFormat surface)))
                (when (ct:cpointer-null surf)
                    (setf surf nil))
                surf))))
                    
(defun set-flags (&rest keyword-args)
    (if (listp (first keyword-args))
        (apply #'logior (first keyword-args))
        (apply #'logior keyword-args)))

(defun init-sdl (&key (flags sdl:SDL_INIT_VIDEO))
    (if (> 0 (sdl:SDL_Init (set-flags flags)))
        nil
        t))

(defun set-videomode (width height &key (bpp 0) (flags sdl:SDL_SWSURFACE))
    "Sets the videomode.
    Returns a new SDL_Surface if successful.
    Returns NIL if failed.
    Use the function (DISPLAY) to retrieve the SDL_Surface returned by set-videomode."
    (let ((surface (sdl:SDL_SetVideoMode width height bpp (set-flags flags))))
        (if (ct:cpointer-null surface)
            (set-display nil)
            (set-display surface))))

(defun new-screen (width height &key (bpp 0) (flags sdl:SDL_SWSURFACE))
    "A synonym for set-videomode.
    Sets the videomode.
    Returns a new SDL_Surface if successful.
    Returns NIL if failed.
    Use the function (DISPLAY) to retrieve the SDL_Surface returned by NEW SCREEN."
    (set-videomode width height :bpp bpp :flags flags))

(defun destruct-sdl ()
    "Shuts down SDL using SDL_Quit"
    (sdl:SDL_Quit))

(defun geterror ()
    "Returns the last error using SDL_GETERROR if any, or returns nil."
    (let ((error-string (SDL:SDL_GETERROR)))
        (if (not (ct:cpointer-null error-string))
            (ct:C-STRING-TO-LISP-STRING error-string)
            nil)))

(defmacro with-init (init-flags &body body)
    "Attempts to initialize SDL using the flags init-flags. 
    Automatically shuts down SDL using SDL_Quit if unsuccessful.
    Test for failure using the function INIT-SUCCESS.
        INIT-SUCCESS returns:
            T if with-init is successful, 
            NIL for failure."
    `(block nil
        (unwind-protect
            (let ((init-succ (init-sdl :flags (list ,@init-flags))))
                (let ((succ t))
                    (if (eql init-succ -1)
                        (setf succ nil))
                    (defun init-success ()
                        succ))
                ,@body)
            (destruct-sdl))))

(defun loadbmp (path)
    "Loads the .BMP file specified in path.
    Returns a new sdl:SDL_Surface if successful.
    Returns nil if unsuccessful."
    (if (stringp path)
        (let ((surface (sdl:SDL_LoadBMP (ct:LISP-STRING-TO-C-STRING path))))
            (when (ct:cpointer-null surface)
                (setf surface nil))
            surface)
        nil))

(defun get-key (keysym)
    "Takes an sdl:SDL_keysym structure and returns SDL_keysym.sym; 
    the SDLKey e.g. SDLK_ESCAPE, SDLK_SPACE etc."
    (ct:cref sdl:SDL_keysym keysym sdl::sym))

(defun is-key (keysym key)
    "Takes an sdl:SDL_keysym structure and returns SDL_keysym.sym; 
    the SDLKey e.g. SDLK_ESCAPE, SDLK_SPACE etc."
    (equal (sdl:get-key keysym) key))


(defun new-event (&optional event-type)
    "Creates a new SDL:SDL_EVENT and sets the type to event-type.
     If no type is specified, the SDL:SDL_EVENT is returned."
    (let ((event (c-types:malloc (c-types:sizeof 'sdl:SDL_Event))))
        (when event-type
            (setf (ct:cref sdl:SDL_Event event sdl::type) event-type))
        event))

(defun new-rect ()
    "Creates a new sdl:SDL_Rect using malloc."
    (ct:malloc (ct:sizeof 'sdl:SDL_Rect)))

(defun pixelformat (surface)
    "Returns the pixelformat of a surface using sdl::format"
    (when (and 
            (ct:cpointerp surface)
            (not (ct:cpointer-null surface)))
        (ct:cref sdl:SDL_Surface surface sdl::format)))


(defun fill-surface (surface &key (r 0) (g 0) (b 0)  (alpha nil) (template NULL))
    "fill SURFACE &key R G B ALPHA (template null)
    Fill SURFACE with the specified color using the keyword parameters R G B and Alpha.
    :template is the fill template."
    (if ALPHA
        (sdl:SDL_FillRect surface template 
            (sdl:SDL_MapRGB (sdl:pixelformat surface) r g b))
        (sdl:SDL_FillRect surface template 
            (sdl:SDL_MapRGBA (sdl:pixelformat surface) r g b ALPHA))))
        
(defun fill-display (&key (r 0) (g 0) (b 0) (template NULL))
    "fill-display &key R G B template
    Fills the display with a color specified using the R G B parameters.
    The keyword :template (SDL_Rect) defaults to NULL."
    (sdl:SDL_FillRect (sdl:display) template 
        (sdl:SDL_MapRGB (sdl:pixelformat (sdl:display)) r g b)))

#|(defun clear-display (&key (r 0) (g 0) (b 0))
    "clear-display &key R G B
    Clears the whole display using the keyword :R :G :B parameters. Color defaults to black."
    (sdl:SDL_FillRect (sdl:display) NULL 
        (sdl:SDL_MapRGB (sdl:pixelformat (sdl:display)) r g b)))
|#

(defun update-display (&key (x 0) (y 0) (w 0) (h 0) (template nil))
    "update-display &key X Y W H
    Updates the screen using the keyword co-orditates :X :Y :W :H, or :template.
    Co-ordinates default to 0, therefore updating the entire screen."
    (if template
        (sdl:SDL_UpdateRect (sdl:display) 
            (sdl:rectangle-x template)
            (sdl:rectangle-y template)
            (sdl:rectangle-w template)
            (sdl:rectangle-h template))
        (sdl:SDL_UpdateRect (sdl:display) x y w h)))

(defun blit-to-display (source &key (template NULL))
    "blit-to-display SOURCE DESTINATION
    Blits the SOURCE surface to the display using SDL_BlitSurface.
    DESTINATION is a SDL_Rect. Only the [x,y] co-ordinates are used to position the source on the display."
    (sdl:blit-to-surface source (sdl:display) :destination-template template))

(defun blit-to-surface (source destination &key (source-template NULL) (destination-template NULL))
    "blit-to-surface SOURCE DESTINATION-COORDS DESTINATION-SURFACE &key SOURCE-RECT
    Blits the SOURCE surface to the DESTINATION-SURFACE using SDL_BlitSurface.
    DESTINATION is a SDL_Rect. Only the [x,y] co-ordinates are used to position the SOURCE on the DESTINATION-SURFACE.
    Use the optional SOURCE-RECT ( SDL_Rect ) to blit a portion of the SOURCE to the DESTINATION-SURFACE."
    (sdl:SDL_BlitSurface source source-template destination destination-template))

(defun set-colorkey (surface r g b &key (accel nil))
    (if (or 
            (null surface)
            (not (ct:cpointerp surface)))
        nil
        (progn
            (if accel
                (setf accel sdl:SDL_RLEACCEL)
                (setf accel 0))
            (sdl:SDL_SetColorKey 
                surface 
                (logior sdl:SDL_SRCCOLORKEY accel) 
                (sdl:SDL_MapRGB (sdl:pixelformat surface) r g b)))))

(defun clear-colorkey (surface &key (accel nil))
    (if (or 
            (null surface)
            (not (ct:cpointerp surface)))
        nil
        (progn
            (if accel
                (setf accel sdl:SDL_RLEACCEL)
                (setf accel 0))
            (sdl:SDL_SetColorKey surface accel 0))))

(defun moveto-rectangle (rectangle dx dy)
    (ct:with-c-struct (var rectangle sdl:SDL_Rect)
        (setf
            sdl::x dx
            sdl::y dy)))

(defun moveby-rectangle (rectangle ax ay)
    (ct:with-c-struct (var rectangle sdl:SDL_Rect)
        (setf 
            sdl::x (+ sdl::x ax)
            sdl::y (+ sdl::y ay))))

(defun rectangle-x (rectangle)
    (ct:cref sdl:SDL_Rect rectangle sdl::x))

(defun rectangle-y (rectangle)
    (ct:cref sdl:SDL_Rect rectangle sdl::y))

(defun rectangle-w (rectangle)
    (ct:cref sdl:SDL_Rect rectangle sdl::w))

(defun rectangle-h (rectangle)
    (ct:cref sdl:SDL_Rect rectangle sdl::h))

(defun surface-w (surface)
    (ct:cref sdl:SDL_Surface surface sdl::w))

(defun surface-h (surface)
    (ct:cref sdl:SDL_Surface surface sdl::h))

(defun set-rectangle (rectangle x1 y1 w1 h1)
    (ct:with-c-struct (var rectangle sdl:SDL_Rect)
        (setf 
            sdl::x x1
            sdl::y y1
            sdl::w w1
            sdl::h h1)))

(defun push-quitevent ()
    "Pushes a new SDL:SDL_EVENT of type SDL:SDL_QUIT onto the event queue."
    (let ((event (new-event sdl:sdl_quit)))
        (sdl:SDL_PushEvent event)))

(defun getvideoinfo (video-info &key info)
    "Returns information from the video hardware on the best/current video mode.
    GETVIDEOINFO <pointer to a SDL:SDL_VIDEOINFO structure> :info <key>
    Use the following keywords to return information about the video hardware 
        :info
            'hw-available
            'wm-available
            'blit-hw
            'blit-hw-cc
            'blit-hw-a
            'blit-sw
            'blit-sw-cc
            'blit-sw-a
            'blit-fill
            'video-mem
            'blit-fill
    e.g. (getvideoinfo *sdl:SDL_VideoInfo* :info 'video-mem)
        returns the amount video memory available."
    (if (ct:cpointer-null video-info)
        nil
        (case info
            (hw-available
                (if (eql 1 (logand 1 (ash (ct:cref sdl:SDL_VideoInfo video-info sdl::int1) 0)))
                    t
                    nil))
            (wm-available
                (if (eql 1 (logand 1 (ash (ct:cref sdl:SDL_VideoInfo video-info sdl::int1) -1)))
                    t
                    nil))
            (blit-hw
                (if (eql 1 (logand 1 (ash (ct:cref sdl:SDL_VideoInfo video-info sdl::int2) -1)))
                    t
                    nil))
            (blit-hw-cc
                (if (eql 1 (logand 1 (ash (ct:cref sdl:SDL_VideoInfo video-info sdl::int2) -2)))
                    t
                    nil))
            (blit-hw-a
                (if (eql 1 (logand 1 (ash (ct:cref sdl:SDL_VideoInfo video-info sdl::int2) -3)))
                    t
                    nil))
            (blit-sw
                (if (eql 1 (logand 1 (ash (ct:cref sdl:SDL_VideoInfo video-info sdl::int2) -4)))
                    t
                    nil))
            (blit-sw-cc
                (if (eql 1 (logand 1 (ash (ct:cref sdl:SDL_VideoInfo video-info sdl::int2) -5)))
                    t
                    nil))
            (blit-sw-a
                (if (eql 1 (logand 1 (ash (ct:cref sdl:SDL_VideoInfo video-info sdl::int2) -6)))
                    t
                    nil))
            (blit-fill
                (if (eql 1 (logand 1 (ash (ct:cref sdl:SDL_VideoInfo video-info sdl::int2) -7)))
                    t
                    nil))
            (video-mem
                (ct:cref sdl:SDL_VideoInfo video-info sdl::video_mem))
            (pixelformat
                (ct:cref sdl:SDL_VideoInfo video-info sdl::vfmt))
            (t
                nil))))

(defun listmodes (mode-flags)
    "Returns a LIST of SDL:SDL_RECT structures for each available screen dimension 
    for the given format and video flags, sorted largest to smallest. 
    Returns NIL if there are no dimensions available for a particular format, 
    or T if any dimension is okay for the given format."
    (let (
            (modes nil)
            (listmodes (sdl:SDL_ListModes (ct:create-foreign-ptr) (set-flags mode-flags))))
        (if (eql (ct:CPOINTER-VALUE listmodes) 4294967295)
            (setf modes t)
            (do ((i 0 (1+ i)))
                ((ct:cpointer-null (ct:cref ((sdl:sdl_rect *) *) listmodes i)) modes)
                (setf modes (cons (ct:cref ((sdl:sdl_rect *) *) listmodes i) modes ))))
        modes))

(defun videomodeok (width height bpp surface-flags)
    "SDL_VideoModeOK returns 0 if the requested mode is not supported under any bit depth, 
    or returns the bits-per-pixel of the closest available mode with the given 
    width, height and requested surface flags."
    (let ((bits-per-pixel (sdl:SDL_VideoModeOK width height bpp (set-flags surface-flags))))
        (if (eql 0 bits-per-pixel)
            nil
            bits-per-pixel)))

(let ((hash-table (make-hash-table #|:test 'equal|#)))
    (defun get-surface-table ()
        hash-table))

(defun add-surface (id surface)
    (if (or 
            (null surface)
            (not (ct:cpointerp surface)))
        nil
        (setf (gethash id (get-surface-table)) surface)))

(defun get-surface (id)
    (gethash id (get-surface-table)))


(defun calculate-timescale (params forms scale)
    `(progn
        (funcall #'(lambda ,params
                ,@forms)
            ,scale)))

(defun expand-activeevent (sdl-event params forms)
    `((eql sdl:SDL_ACTIVEEVENT
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_ActiveEvent ,sdl-event sdl::gain)
            (ct:cref sdl:SDL_ActiveEvent ,sdl-event sdl::state))))

(defun expand-keydown (sdl-event params forms)
    `((eql sdl:SDL_KEYDOWN
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_KeyboardEvent ,sdl-event sdl::state)
            (ct:cref sdl:SDL_keysym (ct:cref sdl:SDL_KeyboardEvent ,sdl-event sdl::keysym) sdl::scancode)
            (ct:cref sdl:SDL_keysym (ct:cref sdl:SDL_KeyboardEvent ,sdl-event sdl::keysym) sdl::sym)
            (ct:cref sdl:SDL_keysym (ct:cref sdl:SDL_KeyboardEvent ,sdl-event sdl::keysym) sdl::mod)
            (ct:cref sdl:SDL_keysym (ct:cref sdl:SDL_KeyboardEvent ,sdl-event sdl::keysym) sdl::unicode))))

(defun expand-keyup (sdl-event params forms)
    `((eql sdl:SDL_KEYUP
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_KeyboardEvent ,sdl-event sdl::state)
            (ct:cref sdl:SDL_keysym (ct:cref sdl:SDL_KeyboardEvent ,sdl-event sdl::keysym) sdl::scancode)
            (ct:cref sdl:SDL_keysym (ct:cref sdl:SDL_KeyboardEvent ,sdl-event sdl::keysym) sdl::sym)
            (ct:cref sdl:SDL_keysym (ct:cref sdl:SDL_KeyboardEvent ,sdl-event sdl::keysym) sdl::mod)
            (ct:cref sdl:SDL_keysym (ct:cref sdl:SDL_KeyboardEvent ,sdl-event sdl::keysym) sdl::unicode))))

(defun expand-mousemotion (sdl-event params forms)
    `((eql sdl:SDL_MOUSEMOTION
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_MouseMotionEvent ,sdl-event sdl::state)
            (ct:cref sdl:SDL_MouseMotionEvent ,sdl-event sdl::x)
            (ct:cref sdl:SDL_MouseMotionEvent ,sdl-event sdl::y)
            (ct:cref sdl:SDL_MouseMotionEvent ,sdl-event sdl::xrel)
            (ct:cref sdl:SDL_MouseMotionEvent ,sdl-event sdl::yrel))))

(defun expand-mousebuttondown (sdl-event params forms)
    `((eql sdl:sdl_mousebuttondown
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_MouseButtonEvent ,sdl-event sdl::button)
            (ct:cref sdl:SDL_MouseButtonEvent ,sdl-event sdl::state)
            (ct:cref sdl:SDL_MouseButtonEvent ,sdl-event sdl::x)
            (ct:cref sdl:SDL_MouseButtonEvent ,sdl-event sdl::y))))

(defun expand-mousebuttonup (sdl-event params forms)
    `((eql sdl:sdl_mousebuttonup
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_MouseButtonEvent ,sdl-event sdl::button)
            (ct:cref sdl:SDL_MouseButtonEvent ,sdl-event sdl::state)
            (ct:cref sdl:SDL_MouseButtonEvent ,sdl-event sdl::x)
            (ct:cref sdl:SDL_MouseButtonEvent ,sdl-event sdl::y))))

(defun expand-joyaxismotion (sdl-event params forms)
    `((eql sdl:SDL_JOYAXISMOTION
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_JoyAxisEvent ,sdl-event sdl::which)
            (ct:cref sdl:SDL_JoyAxisEvent ,sdl-event sdl::axis)
            (ct:cref sdl:SDL_JoyAxisEvent ,sdl-event sdl::value))))

(defun expand-joybuttondown (sdl-event params forms)
    `((eql sdl:SDL_JOYBUTTONDOWN
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_JoyButtonEvent ,sdl-event sdl::which)
            (ct:cref sdl:SDL_JoyButtonEvent ,sdl-event sdl::button)
            (ct:cref sdl:SDL_JoyButtonEvent ,sdl-event sdl::state))))

(defun expand-joybuttonup (sdl-event params forms)
    `((eql sdl:SDL_JOYBUTTONUP
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_JoyButtonEvent ,sdl-event sdl::which)
            (ct:cref sdl:SDL_JoyButtonEvent ,sdl-event sdl::button)
            (ct:cref sdl:SDL_JoyButtonEvent ,sdl-event sdl::state))))

(defun expand-joyhatmotion (sdl-event params forms)
    `((eql sdl:SDL_JOYHATMOTION
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_JoyHatEvent ,sdl-event sdl::which)
            (ct:cref sdl:SDL_JoyHatEvent ,sdl-event sdl::hat)
            (ct:cref sdl:SDL_JoyHatEvent ,sdl-event sdl::value))))

(defun expand-joyballmotion (sdl-event params forms)
    `((eql sdl:SDL_JOYBALLMOTION
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_JoyBallEvent ,sdl-event sdl::which)
            (ct:cref sdl:SDL_JoyBallEvent ,sdl-event sdl::ball)
            (ct:cref sdl:SDL_JoyBallEvent ,sdl-event sdl::xrel)
            (ct:cref sdl:SDL_JoyBallEvent ,sdl-event sdl::yrel))))

(defun expand-videoresize (sdl-event params forms)
    `((eql sdl:SDL_VIDEORESIZE
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_ResizeEvent ,sdl-event sdl::w)
            (ct:cref sdl:SDL_ResizeEvent ,sdl-event sdl::h))))

(defun expand-videoexpose (sdl-event forms)
    `((eql sdl:SDL_VIDEOEXPOSE
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ()
                ,@forms))))

(defun expand-syswmevent (sdl-event forms)
    `((eql sdl:SDL_SYSWMEVENT
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (funcall #'(lambda ()
                ,@forms))))

(defun expand-quit (sdl-event forms quit)
    `((eql sdl:SDL_QUIT
            (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type))
        (setf ,quit (funcall #'(lambda ()
                ,@forms)))))

(defun expand-userevent (sdl-event params forms)
    `((and (>= 
                (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type)
                sdl:SDL_MOUSEMOTION)
            (< 
                (c-types:cref sdl:SDL_Event
                ,sdl-event
                sdl::type)
                (- sdl:SDL_NUMEVENTS 1)))
        (funcall #'(lambda ,params
                ,@forms)
            (ct:cref sdl:SDL_MouseMotionEvent ,sdl-event sdl::type)
            (ct:cref sdl:SDL_MouseMotionEvent ,sdl-event sdl::code)
            (ct:cref sdl:SDL_MouseMotionEvent ,sdl-event sdl::data1)
            (ct:cref sdl:SDL_MouseMotionEvent ,sdl-event sdl::data2))))

(defun expand-idle (forms)
    `(progn
        ,@forms))

(defmacro with-events (&body events)
    "(with-sdl-events
            (:activeevent (gain state)
                t)
            (:keydown (state keysym)
                t)
            (:keyup (state keysm)
                t)
            (:mousemotion (state x y xrel yrel)
                t)
            (:mousebuttondown (button state x y)
                t)
            (:mousebuttonup (button state x y)
                t)
            (:joyaxismotion (which axis value)
                t)
            (:joybuttondown (which button state)
                t)
            (:joybuttonup (which button state)
                t)
            (:joyhatmotion (which hat value)
                t)
            (:joyballmotion (which ball xrel yrel)
                t)
            (:videoresize (w h)
                t)
            (:videoexpose
                t)
            (:syswmevent
                t)
            (:quit 
                t))"
  (let ((quit (gensym)) (sdl-event (gensym)) (poll-event (gensym)) 
            (previous-ticks (gensym)) (current-ticks (gensym)))
     `(let (
                (,sdl-event ,(new-event))
                (,quit nil)
                (,previous-ticks ,(sdl:SDL_GetTicks))
                (,current-ticks ,(sdl:SDL_GetTicks)))
        (do ()
           ((eql ,quit t))
                (do ((,poll-event (sdl:SDL_PollEvent ,sdl-event) (sdl:SDL_PollEvent ,sdl-event)))
                    ((eql ,poll-event 0) 'done)
                    (cond
                        ,@(remove nil (mapcar #'(lambda (event)
                                    (case (first event)
                                        (:activeevent
                                            (expand-activeevent sdl-event (first (rest event)) (rest (rest event))))
                                        (:keydown
                                            (expand-keydown sdl-event (first (rest event)) (rest (rest event))))
                                        (:keyup
                                            (expand-keyup sdl-event (first (rest event)) (rest (rest event))))
                                        (:mousemotion
                                            (expand-mousemotion sdl-event (first (rest event)) (rest (rest event))))
                                        (:mousebuttondown
                                            (expand-mousebuttondown sdl-event (first (rest event)) (rest (rest event))))
                                        (:mousebuttonup
                                            (expand-mousebuttonup sdl-event (first (rest event)) (rest (rest event))))
                                        (:joyaxismotion
                                            (expand-joyaxismotion sdl-event (first (rest event)) (rest (rest event))))
                                        (:joybuttondown
                                            (expand-joybuttondown sdl-event (first (rest event)) (rest (rest event))))
                                        (:joybuttonup
                                            (expand-joybuttonup sdl-event (first (rest event)) (rest (rest event))))
                                        (:joyhatmotion
                                            (expand-joyhatmotion sdl-event (first (rest event)) (rest (rest event))))
                                        (:joyballmotion
                                            (expand-joyballmotion sdl-event (first (rest event)) (rest (rest event))))
                                        (:videoresize
                                            (expand-videoresize sdl-event (first (rest event)) (rest (rest event))))
                                        (:videoexpose
                                            (expand-videoexpose sdl-event (rest event)))
                                        (:syswmevent
                                            (expand-syswmevent sdl-event (rest event)))
                                        (:quit
                                            (expand-quit sdl-event (rest event) quit))
                                        (:userevent
                                            (expand-userevent sdl-event (first (rest event)) (rest (rest event))))))
                                events))))
                (progn
                    (setf ,previous-ticks ,current-ticks)
                    (setf ,current-ticks (sdl:SDL_GetTicks))
                    (set-timescale
                        (/ 
                            (- ,current-ticks ,previous-ticks) 
                            1000)))            
                ,@(remove nil (mapcar #'(lambda (event)
                            (cond
                                ((eql :idle (first event))
                                    (expand-idle (rest event)))))
                        events))))))
    
(provide "SDL")