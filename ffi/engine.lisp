;;; A bouncing ball example.
;;; Blits rectangles of random size to random positions on the screen
;;; Original work: "The Simple Direct Media Layer", Ernest S. Pazera
;;; ( http://www.gamedev.net/reference/programming/features/sdl2/page5.asp)
;;; Conversion by Luke J Crook, luke@balooga.com
;;; 12 June, 2003
;;; Version 0.1

(require 'sdl)
(require 'sdl-util)
(in-package :win)


;;;;;
;;;;; Link list functions

(defstruct (dl (:print-function print-dl))
  prev data next)

(defun print-dl (dl stream depth)
  (declare (ignore depth))
  (format stream "#<DL ~A>" (dl->list dl)))

(defun dl->list (lst)
  (if (dl-p lst)
      (cons (dl-data lst) (dl->list (dl-next lst)))
      lst))

(defun dl-insert (x lst)
  (let ((elt (make-dl :data x :next lst)))
    (when (dl-p lst)
      (if (dl-prev lst)
          (setf (dl-next (dl-prev lst)) elt
                (dl-prev elt) (dl-prev lst)))
      (setf (dl-prev lst) elt))
    elt))

(defun dl-append (x lst)
  (let ((elt (make-dl :data x :prev lst)))
    (when (dl-p lst)
      (if (dl-next lst)
          (setf (dl-prev (dl-next lst)) elt
                (dl-next elt) (dl-next lst)))
      (setf (dl-next lst) elt))
    elt))

(defun dl-list (&rest args)
  (reduce #'dl-insert args
          :from-end t :initial-value nil))

(defun dl-remove (lst)
  (if (dl-prev lst)
      (setf (dl-next (dl-prev lst)) (dl-next lst)))
  (if (dl-next lst)
      (setf (dl-prev (dl-next lst)) (dl-prev lst)))
  (dl-next lst))

(defun dl-nextnode (lst)
    (if (null (dl-next lst))
        (values lst nil)
        (values (dl-next lst) t)))

(defun dl-prevnode (lst)
    (if (null (dl-prev lst))
        (values lst nil)
        (values (dl-prev lst) t)))

(defun dl-find (dl func)
    (let ((obj dl))
        (loop
            (when (null obj) (return nil))
            (if (funcall func obj)
                (return obj)
                (setf obj (dl-next obj))))))

;;;;; End: Link list functions
;;;;;


(defun create-display-list ()
    (let ((display-list nil))
        (defun init-display-list ()
            (setf display-list nil))
        (defun display-list ()
            display-list)))

(defstruct (level)
        zorder
        start
        end)

(defclass sprite ()
    (
        (id :accessor sprite-id :initform nil :initarg :id)
        (x :accessor sprite-x :initform 0 :initarg :x)
        (y :accessor sprite-y :initform 0 :initarg :y)
        (zorder :accessor sprite-zorder :initform 0 :initarg :zorder)))

(defun new-level (zorder)
    (make-level :zorder zorder))

(defun addto-level (level object)
    (cond 
        ((null (level-end level))
            (setf (level-end level) (dl-list object))
            (setf (level-start level) (level-end level)))
        (t
            (setf (level-end level) (dl-append object (level-end level))))))

;Removes a node from the level.
(defun remove-node-from-level (level object)
    (when (null (dl-next object))
        (setf (level-end level) (dl-prev object)))
    (when (null (dl-prev object))
        (setf (level-start level) (dl-next object)))
    (dl-remove object))

;Finds the node containing object, then calls remove-node-from-level
(defun remove-from-level (level object)
    (let ((obj (dl-find (level-start level) 
        #'(lambda (node)
            (if (equal (dl-data node) object)
                node
                nil)))))
        (when obj
            (remove-node-from-level level obj))))

(defun add-level (level objects)
    (cond 
        ((null objects)
            (setf objects (dl-list level)))
        ((null (level-end level))
            (setf (level-end level) (dl-list objects))
            (setf (level-start level) (level-end level)))
        (t
            (setf (level-end level) (dl-append objects (level-end level))))))



(defun new-find-zlevel (zorder)
    #'(lambda (dl)
        (cond 
            ((equal (sprite-id (dl-data dl)) zorder)
                dl
                nil)))

(defun find-zlevel (levels zorder)
    (if (null levels)
        (values nil nil)
        (let ((obj objects))
            (loop
                (when (null obj) (return))
                (cond
                    ((equal zorder (zlevel-zorder (dl-data obj)))
                        (return (values obj t)))
                    ((> zorder (zlevel-zorder (dl-data obj)))
                        (return (values obj nil)))
                    (t
                        (setf obj (dl-next obj))))))))
    



(defun add-zlevel (objects zlevel)
    




(defun add-object (spr)
    (when (null objects)
        (setf objects (new-zlevel (sprite-id spr))))
    (let ((obj (dl-find objects (new-find-zlevel (sprite-id spr)))))
        (if obj
            (
        


(defun add-to (obj l)
    (nconc l (list obj)))

(defstruct node 
    (prev nil)
    (next nil)
    data)

(defun insert (data zorder llist)
    (if (null llist)
        (make-node :data (cons zorder (add-to data nil)))
        (if (eql (first (node-data llist)) zorder)
            (add-to data (node-data llist))
            (if (> (first (node-data llist)) zorder)
                (let ((node (make-node 
                                :data (cons zorder (add-to data (node-data llist)))
                                :next llist
                                :prev (node-prev llist))))
                    (setf (node-prev llist) node))
                (if (null (node-next llist))
                    (let ((node (make-node 
                                :data (cons zorder (add-to data (node-data llist)))
                                :prev llist)))
                        (setf (node-next llist) node))
                    (insert data zorder (node-next llist)))))))


(setf a-list '(1 2 3 4 5))
(setf b-list '(a b c d e))
(setf (cdr a-list) b-list)
(setf a-list nil)

(cdr (car b-list))



#|(defun insert-into (lst node &optional (func #'<))
    (if (null lst)
        (cons node nil)
        (if (funcall func (first lst) node)
            (progn
                (setf (cdr lst) (insert-into (rest lst) node func))
                lst)
            (cons node lst))))
|#

#|(defun insert (lst node zorder &optional (func #'<))
    (if (null lst)
        (cons (list zorder node)  nil)
        (cond 
            ((funcall func (first (first lst)) zorder)
                (setf (cdr lst) (insert (cdr lst) node zorder func))
                lst)
            ((= (first (first lst)) zorder)
                (setf (cdr (first lst)) (insert-into (cdr (first lst)) node func))
                lst)
            (t
                (cons (list zorder node) lst)))))

(defun get-zorder (lst)
    (if (null lst)
        nil
        (first (first lst))))

(defun insert (lst node zorder &optional (func #'<))
    (if (null lst)
        (cons (list zorder node) nil)
        (cond 
            ((funcall func (get-zorder lst) zorder)
                (setf (cdr lst) (insert (cdr lst) node zorder func))
                lst)
            ((= (get-zorder lst) zorder)
                (setf (cdr (first lst)) (insert-into (cdr (first lst)) node func))
                lst)
            (t
                (cons (list zorder node) lst)))))
|#

(defun add-to (lst nodes)
    (cond 
        ((and (null lst) (listp nodes))
            nodes)
        ((null lst)
            (cons nodes nil))
        (t
            (let ((l (last lst)))
                (if (listp nodes)
                    (setf (cdr l) nodes)
                    (setf (cdr l) (cons nodes nil)))))))
        
        
        ((listp nodes)
            (setf lst nodes))
        (t
            (cons (last lst) nodes))))

(defun insert (lst zorder nodes)
    (if (null lst)
        (cons (list 
                zorder
                (add-to nil nodes))
            nil)
        (cond 
            ((< (get-zorder lst) zorder)
                (setf (cdr lst) (insert (cdr lst) zorder nodes))
                lst)
            ((= (get-zorder lst) zorder)
                (setf (cdr (first lst)) (add-to (cdr (first lst)) nodes))
                lst)
            (t
                (cons (list zorder (add-to nil nodes)) lst)))))

;(1 a b c d)
(setf b-list '(2 e f g h))
(last  b-list)

(setf a-list nil)

(setf a-list (insert a-list 1 '(200 300 100 400 500)))

(setf a-list (insert a-list 1 2))

(setf a-list (insert-into a-list 0 #'<))

(untrace insert-into)


a-list


(defclass engine ()
    (
        (surface :accessor engine-surface #| get surface with DISPLAY-SURFACE|#)
        (sprites :accessor engine-sprites :initform (make-instance 'sprites))
        ))



(defclass sprites ()
    (
        (sprite-list :accessor sprites)))

(defun get-zorder (slist)
    (if (null slist)
        nil
        (if (listp slist)
            (first slist))))

(defun add-to (sprites sprite)
    (nconc sprites (list sprite)))

(defun insert-at (slist s z)
    (cond
        ((null slist)
            (list (cons z (add-to nil s))))
        ((listp (first slist))
            (insert-at (first slist) s z))
        ((eql (first slist) z)
            (add-to slist s))
        ((< (first slist) z)
            (add-to (rest slist) s))))
    

(defmethod add-sprite ((sp-list sprites) (s sprite))
    (let ((sprite-list (sprites sp-list)))
        (cond 
            ((if (eql (z-order s) (z-order sprites))))
            ((null sprite-list)
                (setf sprite-list
                    (cons 
                        (z-order s) sprite-list)
                        s))))))
    

(defmethod add-sprite ((sprites sp-list) (s sprite))
    (
    
        

(defmethod set-videosurface ((e engine) s)
    (when (and 
            (not (null s))
            (ct:cpointerp s))
        (setf (engine-surface e) s)))



(defmethod add-sprite ((e engine) (s sprite))
    )            
            
            

(defun bouncing-ball ()
    (let (
            (width 800) (height 600)
            (video-flags (list                  
                    #|sdl:SDL_FULLSCREEN 
                    sdl:SDL_HWSURFACE
                    sdl:SDL_DOUBLEBUF|#
                    sdl:SDL_SWSURFACE))
            (pBitmap nil)
            (rcDst (sdl:new-rect))
            (dy 6) (dx 4)
            (max-right 0) (max-height 0))
       
        (sdl:with-sdl-init (sdl:SDL_INIT_VIDEO)
            (unless (sdl:set-videomode width height :flags video-flags)
                (fformat "FAILED: set-videomode, cannot set the video mode")
                (return))
            
            ; Initialize the engine
            (engine-init)
            ; Set the video surface
            (set-videosurface (get-engine) (display-surface))
            
            (unless (sdl:add-surface 'b-ball (sdl:sdl-DisplayFormat (sdl:sdl-loadbmp "b-ball.bmp")))
                (fformat "Cannot find b-ball.bmp")
                (return))
            
            (setf pBitmap (sdl:get-surface 'b-ball))
                                       
            (with-c-struct (var rcDst sdl:SDL_Rect)
                (setf
                    sdl::w (ct:cref sdl:SDL_Surface pBitmap sdl::w)
                    sdl::h (ct:cref sdl:SDL_Surface pBitmap sdl::h)
                    sdl::x 0
                    sdl::y 0))
        
            (with-c-struct (var rcDst sdl:sdl_rect)
                (setf 
                    max-height (- height sdl::h)
                    max-right (- width sdl::w)))
        
            (sdl:with-sdl-events 
                (:quit 
                    t)
                (:keydown (state keysym)
                    (when (eql (sdl:get-key keysym) sdl:SDLK_ESCAPE)
                        (sdl:push-quitevent)))
                (:idle
                    (sdl:SDL_FillRect (sdl:display-surface) NULL 0)
                
                    (sdl:SDL_BlitSurface pBitmap NULL (sdl:display-surface) rcDst)
                             
                    (with-c-struct (var rcdst sdl:sdl_rect)
                        (incf sdl::x dx)
                        (incf sdl::y dy)
                        (when (or (<= sdl::x 0) (>= sdl::x max-right))
                            (setf dx (- dx)))
                        (when (or (<= sdl::y 0) (>= sdl::y max-height))
                            (setf dy (- dy))))
                                               
                    (sdl:sdl-Flip))))
            
        (unless (sdl:sdl-init-success)
            (sdl:fformat "ERROR: sdl-init FAILED to initialize"))))
                    
;;; Change the current directory to the path where the file "b-ball.bmp" is located.
;;; e.g. use    (ccl:get-current-directory)
;;;             (ccl:set-current-directory "F:/Documents and Settings/Crook/My Documents/dev/lisp/corman-SDL/current")
;;; Run the example using...    
;;; (th:create-thread #'bouncing-ball)

;;; Build the exe using...
;;; (SAVE-APPLICATION "bouncing-ball.exe" 'bouncing-ball :static t)
