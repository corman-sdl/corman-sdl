;;; A bouncing ball example.
;;; Blits rectangles of random size to random positions on the screen
;;; Original work: "The Simple Direct Media Layer", Ernest S. Pazera
;;; ( http://www.gamedev.net/reference/programming/features/sdl2/page5.asp)
;;; Conversion by Luke J Crook, luke@balooga.com
;;; 12 June, 2003
;;; Version 0.1
;;; Version 0.2, 04.04.06, supports transparency

(require :mp)
(require 'sdl)
(require 'sdl-util)
(in-package :win)

(defun bouncing-ball ()
    (let (
            (width 800) (height 600)
            (video-flags (list sdl:SDL_SWSURFACE))
            (pBitmap nil) (tempBitmap nil)
            (rcDst (sdl:new-rect))
            (dy 6) (dx 4)
            (max-right 0) (max-height 0))
       
        (sdl:with-init (sdl:SDL_INIT_VIDEO)
            (unless (sdl:set-videomode width height :flags video-flags)
                (sdl:fformat "FAILED: set-videomode, cannot set the video mode")
                (return))
                  
            (setf tempBitmap (sdl:loadbmp "b-ball.bmp"))
            (unless tempBitmap
                (sdl:fformat "ERROR: Cannot find \"b-ball.bmp\" in directory ~A~%" (ccl:get-current-directory))
                (return))
            
            ;Now we make all black pixels transparent..
            ;First, set the color black (0, 0, 0) to be the transparent pixel using SDL_SetColorKey
            (sdl:set-colorkey tempBitmap 0 0 0 :accel t)
            ;Now call (sdl:displayformat) in order to convert the surface 
            ;to native SDL format for fast blitting.
            (setf pBitmap (sdl:displayformat tempBitmap))
                                                 
            (sdl:moveto-rectangle rcDst 0 0)
            
            (setf 
                max-height (- height (sdl:surface-h pBitmap))
                max-right (- width (sdl:surface-w pBitmap)))
                    
            (sdl:with-events 
                (:quit t)
                (:keydown (state scancode key mod unicode)
                    (when (= key sdl:SDLK_ESCAPE)
                        (sdl:push-quitevent)))
                (:idle
                    (sdl:fill-display :r 255 :g 255 :b 255)
                                                
                    (sdl:moveby-rectangle rcDst dx dy)
                    (when (or (<= (sdl:rectangle-x rcDst) 0) (>= (sdl:rectangle-x rcDst) max-right))
                        (setf dx (- dx)))
                    (when (or (<= (sdl:rectangle-y rcDst) 0) (>= (sdl:rectangle-y rcDst) max-height))
                        (setf dy (- dy)))
                    
                    (sdl:blit-to-display pBitmap :template rcDst)
                    (sdl:Flip))))
            
        (unless (sdl:init-success)
            (sdl:fformat "ERROR: sdl:with-init FAILED to initialize"))))
                    
;;; Change the current directory to the path where the file "b-ball.bmp" is located.
;;; e.g. use    (ccl:get-current-directory)
;;;             (ccl:set-current-directory "F:/Documents and Settings/Crook/My Documents/dev/lisp/corman-SDL/current")
;;;             (ccl:set-current-directory "C:/Documents and Settings/00u4440/My Documents/dev/test")
;;;             (ccl:set-current-directory "C:/Documents and Settings/00u4440/My Documents/dev")
;;; Run the example using...    
;;; (setf bball (mp:process-run-function "bouncing-ball" #'bouncing-ball))
;;; (mp:proc)

;;; Build the exe using...
;;; (SAVE-APPLICATION "bouncing-ball.exe" 'bouncing-ball :static t)
