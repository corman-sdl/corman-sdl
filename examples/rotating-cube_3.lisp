;;; A rotating cube example.
;;; Taken from the SDL example at http://sdldoc.csn.ul.ie/guidevideoopengl.php
;;; Author: Luke J Crook, luke@balooga.com
;;;
;;; Operation:
;;; - Press any key (except Escape) to pause/restart rotation.
;;; - Press Escape to exit.
;;; - Left-click and use the mouse to rotate the cube around the x/y axises.
;;; 
;;; Issues:
;;; - Rotation is not scaled to time but is based on frame-rate. Therefore the rotation is crazy-fast on decent
;;;   hardware.
;;
;; 16 Feb, 2004

(require :mp)
(require 'sdl)
(require 'sdl-util)
(require 'opengl)
(require 'opengl_)
(in-package :win)

(ct:defctype vertex-arrayf (:single-float 3))
(ct:defctype colour-arrayu (GLubyte 4))

(defparameter *angle* 0)
(defparameter *rotate* t)
(defparameter *rotatex* 0.0)
(defparameter *rotatey* 0.0)
(defparameter *rotatez* 0.0)

; Many thanks, Chris Double
(defmacro with-glBegin (type &body body)
    `(progn
        (glBegin ,type)
        (unwind-protect
            (progn ,@body)
            (glEnd))))

(defun opengl-set-attribute (attribute)
    (apply #'sdl:SDL_GL_SetAttribute attribute))

(defun opengl-set-attributes (attributes)
    (mapcar #'opengl-set-attribute attributes))

;; Create a hash table to store the color palette for each vertices
(let ((hash-table (make-hash-table #|:test 'equal|#)))
    (defun get-palette-table ()
        hash-table))

(defun add-color (id color)
    (if (or 
            (null color)
            (not (ct:cpointerp color)))
        nil
        (setf (gethash id (get-palette-table)) color)))

(defun get-color (id)
    (gethash id (get-palette-table)))


(defun create-palette ()
    (let ((palette '(
                    (red 255 0 0 255)
                    (green 0 255 0 255)
                    (blue 0 0 255 255)
                    (white 255 255 255 255)
                    (yellow 0 255 255 255)
                    (black 0 0 0 255)
                    (orange 255 255 0 255)
                    (purple 255 0 255 0))))
        
        (mapcar #'(lambda (color)
                (let (
                        (color-array (ct:malloc (ct:sizeof 'colour-arrayu)))
                        (col (first color))
                        (rgb (rest color)))
                    
                    (add-color col color-array)
                    (sdl:for i 0 3
                        (setf (ct:cref colour-arrayu color-array i) (nth i rgb)))))
            palette)))

(defun create-object ()
    (let ((cube '(
                    (-1.0 -1.0 1.0)
                    (1.0 -1.0 1.0)
                    (1.0 1.0 1.0)
                    (-1.0 1.0 1.0)
                    (-1.0 -1.0 -1.0)
                    (1.0 -1.0 -1.0)
                    (1.0 1.0 -1.0)
                    (-1.0 1.0 -1.0)))
            (vertices nil)
            (colors nil)
            (polys nil))
    
        ;;Create the vertices
        (setf vertices 
            (mapcar #'(lambda (vertex)
                (let ((v-array (ct:malloc (ct:sizeof 'vertex-arrayf))))
                    (sdl:for i 0 2
                        (setf (ct:cref vertex-arrayf v-array i) (nth i vertex)))
                    v-array))
            cube))
                
        ;;Assign a color to each vertex. Assignment is based on position in the list, 
        ;;so the first color in the colors list is assigned to the first vertex in the vertices list.
        (setf colors (list 
                (get-color 'red)
                (get-color 'green)
                (get-color 'blue)
                (get-color 'white)
                (get-color 'yellow)
                (get-color 'black)
                (get-color 'orange)
                (get-color 'purple)))
        
        ;;Create the polygons
        (setf polys '(
                (0 1 2)
                (0 2 3)
                (1 5 6)
                (1 6 2)
                (5 4 7)
                (5 7 6)
                (4 0 3)
                (4 3 7)
                (3 2 6)
                (3 6 7)
                (1 0 4)
                (1 4 5)))
        
        ;Return an 'object' with the vertices, color assignment and list of polygons
        `(
            (vertices ,vertices)
            (colors ,colors)
            (polys ,polys))))

(defun assoc-data (key assoc-list)
    (first (rest (assoc key assoc-list))))

(defun draw-screen (object)
    (if (not (null *rotate*))
        (setf *angle* (+ *angle* 1)))
    
    (glClear (logior GL_COLOR_BUFFER_BIT GL_DEPTH_BUFFER_BIT)) 
    (glMatrixMode GL_MODELVIEW)
    (glLoadIdentity)
    
    (glTranslatef 0.0 0.0 -5.0)
    (glRotatef (+ (coerce *angle* 'single-float) (coerce *rotatex* 'single-float)) 0.0 1.0 0.0)
    (glRotatef (coerce *rotatey* 'single-float) 1.0 0.0 0.0)    
    (glRotatef (coerce *rotatez* 'single-float) 0.0 0.0 1.0)
           
    (if (> *angle* 360.0)
        (setf *angle* 0.0))
    
    (with-glBegin GL_TRIANGLES
        
        (let (
                (vertices (assoc-data 'vertices object))
                (colors (assoc-data 'colors object))
                (polys (assoc-data 'polys object)))                   
            (mapcar #'(lambda (poly)
                    (sdl:for i 0 2
                        (glColor4ubv (nth (nth i poly) colors))
                        (glVertex3fv (nth (nth i poly) vertices))))
                polys)))
    
    (sdl:SDL_GL_SwapBuffers))
          
(defun setup-opengl (width height)
    (let ((ratio (coerce (/ width height) 'double-float)))
        (opengl-set-attributes 
            (list
                (list sdl:SDL_GL_RED_SIZE 5)
                (list sdl:SDL_GL_GREEN_SIZE 5)
                (list sdl:SDL_GL_BLUE_SIZE 5)
                (list sdl:SDL_GL_DEPTH_SIZE 16)
                (list sdl:SDL_GL_DOUBLEBUFFER 1)))
        
        (glShadeModel GL_SMOOTH)
        (glCullFace GL_BACK)
        (glFrontFace GL_CCW)
        (glEnable GL_CULL_FACE)
        (glClearColor 0.0 0.0 0.0 0.0)
        (glViewport 0 0 width height)
        (glMatrixMode GL_PROJECTION)
        (glLoadIdentity)
        (gluPerspective 60.0d0 ratio 1.0d0 1024.0d0)))

(defun rotating-cube ()
    (let (
            (width 640) (height 480) (video-flags (list sdl:SDL_SWSURFACE sdl:SDL_OPENGL))
            (cube nil))
        
        (sdl:with-init (sdl:SDL_INIT_VIDEO)
            
            (unless (sdl:set-videomode width height :flags video-flags)
                (fformat "FAILED: set-videomode, cannot set the video mode")
                (return))
            
            (setup-opengl width height)
            (create-palette)
            (setf cube (create-object))
            
            (sdl:with-events
                (:quit t)
                (:keydown (state keysym)
                    (when (sdl:is-key keysym sdl:SDLK_ESCAPE)
                        (sdl:push-quitevent))
                    (when (sdl:is-key keysym sdl:SDLK_SPACE)
                        (setf *rotate* (not *rotate*))))
                (:mousemotion (state x y xrel yrel)
                    (cond
                        ((eql state 1)
                            (setf *rotatex* (+ *rotatex* xrel))
                            (setf *rotatey* (+ *rotatey* yrel )))))
                (:idle
                    (draw-screen cube))))
        
        (unless (sdl:init-success)
            (sdl:fformat "ERROR: sdl-init FAILED to initialize"))))
    
;;; Run the example using...    
;;; (setf cube (mp:process-run-function "rotating-cube" #'rotating-cube))
;;; (mp:proc)

;;; Build the exe using...
;;; (SAVE-APPLICATION "rotating-cube.exe" 'rotating-cube :static t)
