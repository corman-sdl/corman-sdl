;;; The famous OpenGL gears example, by Brian Paul.
;;; Corman Common Lisp and SDL Conversion by: Luke J Crook, luke@balooga.com
;;; 06 December, 2003 Version 1.1
;;; 15 January, 2004 Version 1.2
;;;     - Replaced the with-sdl-events macro with a much better version.
;;;
;;; The Author is not responsible for
;;;   any damage to hardware, loss of data, weight gain, hair loss etc. Use at your own risk.
;;;
;;; Operation:
;;; - Click & hold left mouse button and move mouse to rotate the gears in the x/y planes
;;; 
;;; Issues:
;;; - The movement of the gears is independent of frame rate. Hardware speed only effects 
;;;   the frames per second, so the faster the hardware the smoother the animation.
;;; - Do NOT attempt to run two instances of this program from within Corman Lisp simultaneously. 
;;;   The SDL library is currently limited to a single instance only. Only a single OpenGL window may 
;;;   be open at any one time. SDL is a GAMES library, not a general purpose windowing library.

(require :mp)
(require :sdl)
(require :sdl-util)
(require :opengl)
(require :opengl_)
(in-package :win)

(ct:defctype GLfloat-4 (GLfloat 4))

;; Generic parameters
(defparameter *world-ticks* 15) ; decouples any rotation or movement from the speed of the hardware. 
(defparameter *timescale* 1)
(defparameter *fps-display-interval* 1000)    ; print the average frames per second to the CCL console. 
                                           ; (in milliseconds. 0 == do not print)
(defparameter *move-delay* 0)   ; Inserts a delay of n milliseconds between each draw cycle. For testing.
                                ;(0 == no delay)

;; Application specific parameters
(defparameter *screen-width* 640)
(defparameter *screen-height* 480)
(defparameter *opengl-attributes*
    (list
        (list sdl:SDL_GL_RED_SIZE 5)
        (list sdl:SDL_GL_GREEN_SIZE 5)
        (list sdl:SDL_GL_BLUE_SIZE 5)
        (list sdl:SDL_GL_DEPTH_SIZE 16)
        (list sdl:SDL_GL_DOUBLEBUFFER 1)))
(defparameter *gear1* nil)
(defparameter *gear2* nil)
(defparameter *gear3* nil)
(defparameter *angle* 0)        ; Stores the angle of rotation for the gears.
(defparameter *view_rotx* 20.0)
(defparameter *view_roty* 30.0)
(defparameter *view_rotz* 0.0)
(defparameter *draw-outline* nil) ; Draws a white wireframe outline around each polygon

;;; Begin macro definitions.
; Many thanks, Chris Double
(defmacro with-glBegin (type &body body)
    `(progn
        (glBegin ,type)
        (unwind-protect
            (progn ,@body)
            (glEnd))))

(defmacro with-glPushMatrix (&body body)
    `(progn
        (glPushMatrix)
        (unwind-protect
            (progn ,@body)
            (glPopMatrix))))
 
;;; End macro definitions
;;;

(defun opengl-set-attribute (attribute)
    (apply #'sdl:SDL_GL_SetAttribute attribute))

(defun opengl-set-attributes (attributes)
    (mapcar #'opengl-set-attribute attributes))

;;;
;;; End SDL Events

(defun create-gear (inner_radius outer_radius width teeth tooth_depth)
    (let* (
            (M_PI (/ 22 7))
            (r0 inner_radius)
            (r1 (/ (- outer_radius tooth_depth) 2.0))
            (r2 (/ (+ outer_radius tooth_depth) 2.0))
            (angle 0.0)
            (da (/ (/ (* 2.0 M_PI) teeth) 4.0))
            (u 0.0)
            (v 0.0)
            (len 0.0))
        
        (glShadeModel GL_FLAT)
        
        (glNormal3f 0.0 0.0 1.0)
        
        ;draw front face
        (with-glBegin GL_QUAD_STRIP
            (sdl:for i 0 teeth
                (setf angle (/ (* i 2.0 M_PI) teeth))
                (glVertex3f 
                    (coerce (* r0 (cos angle)) 'single-float)
                    (coerce (* r0 (sin angle)) 'single-float)
                    (coerce (* width 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r1 (cos angle)) 'single-float)
                    (coerce (* r1 (sin angle)) 'single-float)
                    (coerce (* width 0.5) 'single-float))
                (if (< i teeth)
                    (progn
                        (glVertex3f 
                            (coerce (* r0 (cos angle)) 'single-float)
                            (coerce (* r0 (sin angle)) 'single-float)
                            (coerce (* width 0.5) 'single-float))
                        (glVertex3f 
                            (coerce (* r1 (cos (+ (* 3 da) angle))) 'single-float)
                            (coerce (* r1 (sin (+ (* 3 da) angle))) 'single-float)
                            (coerce (* width 0.5) 'single-float))))))
        
        ;draw front sides of teeth
        (with-glBegin GL_QUADS
            (setf da 
                (/ (/ (* 2.0 M_PI) teeth) 
                    4.0))            
            (sdl:for i 0 teeth
                (setf angle (/ (* i 2.0 M_PI) teeth))
                (glVertex3f 
                    (coerce (* r1 (cos angle)) 'single-float)
                    (coerce (* r1 (sin angle)) 'single-float)
                    (coerce (* width 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r2 (cos (+ angle da))) 'single-float)
                    (coerce (* r2 (sin (+ angle da))) 'single-float)
                    (coerce (* width 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r2 (cos (+ angle (* 2 da)))) 'single-float)
                    (coerce (* r2 (sin (+ angle (* 2 da)))) 'single-float)
                    (coerce (* width 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r1 (cos (+ angle (* 3 da)))) 'single-float)
                    (coerce (* r1 (sin (+ angle (* 3 da)))) 'single-float)
                    (coerce (* width 0.5) 'single-float))))
        
        (glNormal3f 0.0 0.0 (- 1.0))
        
        ;draw back face
        (with-glBegin GL_QUAD_STRIP
            (sdl:for i 0 teeth
                (setf angle (/ (* i 2.0 M_PI) teeth))
                (glVertex3f 
                    (coerce (* r1 (cos angle)) 'single-float)
                    (coerce (* r1 (sin angle)) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r0 (cos angle)) 'single-float)
                    (coerce (* r0 (sin angle)) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))
                (if (< i teeth)
                    (progn
                        (glVertex3f 
                            (coerce (* r1 (cos (+ (* 3 da) angle))) 'single-float)
                            (coerce (* r1 (sin (+ (* 3 da) angle))) 'single-float)
                            (coerce (* (- width) 0.5) 'single-float))
                        (glVertex3f 
                            (coerce (* r0 (cos angle)) 'single-float)
                            (coerce (* r0 (sin angle)) 'single-float)
                            (coerce (* (- width) 0.5) 'single-float))))))

        ;draw back sides of teeth
        (with-glBegin GL_QUADS
            (setf da 
                (/ (/ (* 2.0 M_PI) teeth) 
                    4.0))            
            (sdl:for i 0 teeth
                (setf angle (/ (* i 2.0 M_PI) teeth))
                (glVertex3f 
                    (coerce (* r1 (cos (+ angle (* 3 da)))) 'single-float)
                    (coerce (* r1 (sin (+ angle (* 3 da)))) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r2 (cos (+ angle (* 2 da)))) 'single-float)
                    (coerce (* r2 (sin (+ angle (* 2 da)))) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r2 (cos (+ angle da))) 'single-float)
                    (coerce (* r2 (sin (+ angle da))) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r1 (cos angle)) 'single-float)
                    (coerce (* r1 (sin angle)) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))))
        
        ;draw outward faces of teeth
        (with-glBegin GL_QUAD_STRIP
            (sdl:for i 0 teeth
                (setf angle (/ (* i 2.0 M_PI) teeth))            
                (glVertex3f 
                    (coerce (* r1 (cos angle)) 'single-float)
                    (coerce (* r1 (sin angle)) 'single-float)
                    (coerce (* width 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r1 (cos angle)) 'single-float)
                    (coerce (* r1 (sin angle)) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))
                (setf u (- 
                        (* r2 (cos (+ angle da)))
                        (* r1 (cos angle))))
                (setf v (- 
                        (* r2 (sin (+ angle da)))
                        (* r1 (sin angle))))
                
                (setf len (sqrt (+ 
                            (* u u) 
                            (* v v))))
                (setf u (/ u len))
                (setf v (/ v len))
    
                (glNormal3f 
                    (coerce v 'single-float)
                    (coerce (- u) 'single-float)
                    (coerce 0.0 'single-float))
                (glVertex3f 
                    (coerce (* r2 (cos (+ angle da))) 'single-float)
                    (coerce (* r2 (sin (+ angle da))) 'single-float)
                    (coerce (* width 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r2 (cos (+ angle da))) 'single-float)
                    (coerce (* r2 (sin (+ angle da))) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))
                (glNormal3f 
                    (coerce (cos angle) 'single-float)
                    (coerce (sin angle) 'single-float)
                    (coerce 0.0 'single-float))
                (glVertex3f 
                    (coerce (* r2 (cos (+ angle (* 2 da)))) 'single-float)
                    (coerce (* r2 (sin (+ angle (* 2 da)))) 'single-float)
                    (coerce (* width 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r2 (cos (+ angle (* 2 da)))) 'single-float)
                    (coerce (* r2 (sin (+ angle (* 2 da)))) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))
                (setf u (-
                        (* r1 (cos (+ angle (* 3 da))))
                        (* r2 (cos (+ angle (* 2 da))))))
                (setf v (-
                        (* r1 (sin (+ angle (* 3 da))))
                        (* r2 (sin (+ angle (* 2 da))))))
                (glNormal3f 
                    (coerce v 'single-float)
                    (coerce (- u) 'single-float)
                    (coerce 0.0 'single-float))
                (glVertex3f 
                    (coerce (* r1 (cos (+ angle (* 3 da)))) 'single-float)
                    (coerce (* r1 (sin (+ angle (* 3 da)))) 'single-float)
                    (coerce (* width 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r1 (cos (+ angle (* 3 da)))) 'single-float)
                    (coerce (* r1 (sin (+ angle (* 3 da)))) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))
                (glNormal3f 
                    (coerce (cos angle) 'single-float)
                    (coerce (sin angle) 'single-float)
                    (coerce 0.0 'single-float)))
            
            (glVertex3f 
                (coerce (* r1 (cos 0)) 'single-float)
                (coerce (* r1 (sin 0)) 'single-float)
                (coerce (* width 0.5) 'single-float))
            (glVertex3f 
                (coerce (* r1 (cos 0)) 'single-float)
                (coerce (* r1 (sin 0)) 'single-float)
                (coerce (* (- width) 0.5) 'single-float)))
        
        (glShadeModel GL_SMOOTH)
        
        ;draw inside radius cylinder */
        (with-glBegin GL_QUAD_STRIP
            (sdl:for i 0 teeth
                (setf angle (/ (* i 2.0 M_PI) teeth))
                (glNormal3f 
                    (coerce (- (cos angle)) 'single-float)
                    (coerce (- (sin angle)) 'single-float)
                    (coerce 0.0 'single-float))
                (glVertex3f 
                    (coerce (* r0 (cos angle)) 'single-float)
                    (coerce (* r0 (sin angle)) 'single-float)
                    (coerce (* (- width) 0.5) 'single-float))
                (glVertex3f 
                    (coerce (* r0 (cos angle)) 'single-float)
                    (coerce (* r0 (sin angle)) 'single-float)
                    (coerce (* width 0.5) 'single-float))))))
    
(defun move-objects ()
    ; Insert a delay between screen updates. Used in testing to simulate 
    ; heavy processor load / varying frame rates.
    (when (> *move-delay* 0)
        (sdl:SDL_Delay *move-delay*))
        
    (incf *angle* (* 1 *timescale*)))

(defun draw-objects ()
    (with-glPushMatrix
;        (glRotatef (coerce *view_rotx* 'single-float) 1.0 0.0 0.0)
;        (glRotatef (coerce *view_roty* 'single-float) 0.0 1.0 0.0)
        (glRotatef (coerce *view_rotx* 'single-float) 0.0 1.0 0.0)
        (glRotatef (coerce *view_roty* 'single-float) 1.0 0.0 0.0)    
        (glRotatef (coerce *view_rotz* 'single-float) 0.0 0.0 1.0)
        
        (with-glPushMatrix
            (glTranslatef (- 3.0) (- 2.0) 0.0)
            (glRotatef (coerce *angle* 'single-float) 0.0 0.0 1.0)
            (glCallList *gear1*))
        
        (with-glPushMatrix
            (glTranslatef 3.1 (- 2.0) 0.0)
            (glRotatef (coerce (- (* (- 2.0) *angle*) 9.0) 'single-float) 0.0 0.0 1.0)
            (glCallList *gear2*))
        
        (with-glPushMatrix
            (glTranslatef (- 3.1) 4.2 0.0)
            (glRotatef (coerce (- (* (- 2.0) *angle*) 25.0) 'single-float) 0.0 0.0 1.0)
            (glCallList *gear3*))))

(defun draw-screen ()
    (glClear (sdl:set-flags GL_COLOR_BUFFER_BIT GL_DEPTH_BUFFER_BIT))
    
    (glEnable GL_LIGHTING)
    (glEnable GL_LIGHT0)
    
    (when *draw-outline*
        (glEnable GL_POLYGON_OFFSET_FILL)
        (glPolygonOffset 1.0 1.0))
    
    (draw-objects)
    
    (when *draw-outline*
        (glDisable GL_POLYGON_OFFSET_FILL)
        (glDisable GL_LIGHTING)
        (glDisable GL_LIGHT0)
        (glColor3f 1.0 1.0 1.0)
        (glPolygonMode GL_FRONT_AND_BACK GL_LINE)
    
        (draw-objects)
            
        (glPolygonMode GL_FRONT_AND_BACK GL_FILL))
    
    (sdl:SDL_GL_SwapBuffers))
          
(defun setup-opengl (width height)
    (let
        ((screen-ratio (coerce (/ height width) 'double-float)))
        
        (glViewport 0 0 width height)
        (glMatrixMode GL_PROJECTION)
        (glLoadIdentity)
        (glFrustum 
            (- 1.0d0) 
            1.0d0 
            (- screen-ratio) 
            screen-ratio 
            5.0d0 
            60.0d0)
        (glMatrixMode GL_MODELVIEW)
        (glLoadIdentity)
        (glTranslatef 0.0 0.0 (- 40.0))))

(defun create-display-lists ()
    (let (
            (light-position (ct:malloc (ct:sizeof 'GLfloat-4)))
            (red (ct:malloc (ct:sizeof 'GLfloat-4)))
            (green (ct:malloc (ct:sizeof 'GLfloat-4)))        
            (blue (ct:malloc (ct:sizeof 'GLfloat-4))))
            
        (setf (ct:cref GLFloat-4 light-position 0) 5.0)
        (setf (ct:cref GLFloat-4 light-position 1) 5.0)
        (setf (ct:cref GLFloat-4 light-position 2) 10.0)
        (setf (ct:cref GLFloat-4 light-position 3) 0.0)
    
        (setf (ct:cref GLfloat-4 red 0) 0.8)
        (setf (ct:cref GLfloat-4 red 1) 0.1)
        (setf (ct:cref GLfloat-4 red 2) 0.0)
        (setf (ct:cref GLfloat-4 red 3) 1.0)
        
        (setf (ct:cref GLfloat-4 green 0) 0.0)
        (setf (ct:cref GLfloat-4 green 1) 0.8)
        (setf (ct:cref GLfloat-4 green 2) 0.2)
        (setf (ct:cref GLfloat-4 green 3) 1.0)
    
        (setf (ct:cref GLfloat-4 blue 0) 0.2)
        (setf (ct:cref GLfloat-4 blue 1) 0.2)
        (setf (ct:cref GLfloat-4 blue 2) 1.0)
        (setf (ct:cref GLfloat-4 blue 3) 1.0)    
    
        (glLightfv GL_LIGHT0 GL_POSITION light-position)
   
        (glEnable GL_CULL_FACE)
        (glEnable GL_DEPTH_TEST)
    
        ;Create the gears
        (setf *gear1* (glGenLists 1))
        (glNewList *gear1* GL_COMPILE)
        (glMaterialfv GL_FRONT GL_AMBIENT_AND_DIFFUSE red)
        (create-gear 1.0 8.0 1.0 20 0.7)
        (glEndList)
    
        (setf *gear2* (glGenLists 1))
        (glNewList *gear2* GL_COMPILE)
        (glMaterialfv GL_FRONT GL_AMBIENT_AND_DIFFUSE green)
        (create-gear 0.5 4.0 2.0 10 0.7)
        (glEndList)
    
        (setf *gear3* (glGenLists 1))
        (glNewList *gear3* GL_COMPILE)
        (glMaterialfv GL_FRONT GL_AMBIENT_AND_DIFFUSE blue)
        (create-gear 1.3 4.0 0.5 10 0.7)
        (glEndList)
    
        (glEnable GL_NORMALIZE)))
    
(defun openGL-example-1 ()
    (let (
            (video-flags (list sdl:SDL_OPENGL)))
            
        (sdl:with-init (sdl:SDL_INIT_VIDEO)
            (opengl-set-attributes *opengl-attributes*)
            (unless (sdl:set-videomode *screen-width* *screen-height* :flags video-flags)
                (sdl:fformat "FAILED: set-videomode, cannot set the video mode")
                (return))
            
            (setup-opengl *screen-width* *screen-height*)
    
            (create-display-lists)
            
            (sdl:with-events
                (:quit t)
                (:keydown (state keysym)
                    (when (eql (sdl:get-key keysym) sdl:SDLK_ESCAPE)
                        (sdl:push-quitevent)))
                (:mousemotion (state x y xrel yrel)
                    (cond
                        ((eql state 1)
                            (setf *view_rotx* (+ *view_rotx* xrel))
                            (setf *view_roty* (+ *view_roty* yrel )))))
                (:idle             
                    (move-objects)
                    (draw-screen))))
        
        (unless (sdl:init-success)
            (sdl:fformat "ERROR: sdl:with-init FAILED to initialize"))))

;;; Run the example using...    
;;; (setf gears (mp:process-run-function "openGL-example-1" #'openGL-example-1))

;;; Build the exe using...
;;; (SAVE-APPLICATION "opengl-gears.exe" 'opengl-example-1 :static t)

;;; Some parameters...
;;; (setf *timescale* 1)
;;; (setf *fps-display-interval* 1000)
;;; (setf *draw-outline* t)