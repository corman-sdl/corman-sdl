;;; A random rectangle example.
;;; Blits rectangles of random size to random positions on the screen
;;; Original work: "The Simple Direct Media Layer", Ernest S. Pazera
;;; ( http://www.gamedev.net/reference/programming/features/sdl2/page5.asp )
;;; Conversion by Luke J Crook, luke@balooga.com
;;; Version 0.1, 08 June, 2003
;;; Version 0.2, 01 February, 2004
;;; Version 0.4, 02 April, 2004

(require :mp)
(require 'sdl)
(require 'sdl-util)
(in-package :win)

(defun random-rects ()
    (let (
            (width 640) (height 480) (screen-flags (list sdl:SDL_SWSURFACE))
            (rectangle (sdl:new-rect)))
        
        ;Initialize Systems
        ;(documentation 'sdl:with-init 'function) for a description of how to use it.
        ;sdl:with-init will initialize the SDL library and 
        ;automatically call sdl:SDL_Quit upon exit (or if something goes wrong in the body).
        (sdl:with-init (sdl:SDL_INIT_VIDEO)
            
            ;Create the display window
            ;Use (sdl:display-surface) to retrieve the surface created by (sdl:set-videomode)
            (unless (sdl:set-videomode width height :flags screen-flags)
                (fformat "FAILED: set-videomode, cannot set the video mode")
                (return))
            
            ;Main SDL loop / Message Pump
            ;(documentation 'sdl:with-events 'function) for a description of how to use it.
            (sdl:with-events
                (:quit t)
                (:keydown (state scancode key mod unicode)
                    (when (= key sdl:SDLK_ESCAPE)
                        (sdl:push-quitevent)))
                (:idle
                    ;Set up the random rectangle
                    (ct:with-c-struct (x rectangle sdl:sdl_rect)
                        (setf 
                            sdl::x (random width) 
                            sdl::y (random height)
                            sdl::w (random (- width sdl::x))
                            sdl::h (random (- height sdl::y))))
                    
                    ;'Render' the rectangle to the display by:
                    ;Filling the display with a random color, 
                    ;using the [x,y,w,h] of the rectangle as a template
                    (sdl:fill-display :r (random 256) :g (random 256) :b (random 256) :template rectangle)
                    
                    ;Use sdl:flip or sdl:update-display to update the screen.
                    ; Here, sdl:flip updates the entire display whereas
                    ; sdl:update-display updates only that portion of the display that has been modified.
                    ; sdl:update-display is therefore much faster.
                    
                    ;(sdl:Flip)                    
                    
                    (sdl:update-display :template rectangle)))
        
        ;sdl:init-success will check to see if sdl:with-init was initialized correctly.
        (unless (sdl:init-success)
            (fformat "ERROR: sdl:with-init FAILED to initialize")))))
                            
;;; Run the example using...    
;;; (setf rects (mp:process-run-function "random-rects" #'random-rects))
;;; (mp:proc)

;;; Build the exe using...
;;; (SAVE-APPLICATION "random-rects.exe" 'random-rects :static t)