;;;;; Copyright (c) 2003-2004, Luke J Crook
;;;;; All rights reserved.
(require 'sdl)
(require 'sdl-util)
(in-package :win)


;;;;;
;;;;; Link list functions

(defstruct (dl (:print-function print-dl))
    "Linked list node
    dl-prev, dl-data, dl-next"
  prev data next)

(defun print-dl (dl stream depth)
  (declare (ignore depth))
  (format stream "#<DL ~A>" (dl->list dl)))

(defun dl->list (lst)
  (if (dl-p lst)
      (cons (dl-data lst) (dl->list (dl-next lst)))
      lst))

(defun dl-insert (x lst)
    "Insert the item into the list before the node"
  (let ((elt (make-dl :data x :next lst)))
    (when (dl-p lst)
      (if (dl-prev lst)
          (setf (dl-next (dl-prev lst)) elt
                (dl-prev elt) (dl-prev lst)))
      (setf (dl-prev lst) elt))
    elt))

(defun dl-append (x lst)
    "Insert the item into the list after the node"
  (let ((elt (make-dl :data x :prev lst)))
    (when (dl-p lst)
      (if (dl-next lst)
          (setf (dl-prev (dl-next lst)) elt
                (dl-next elt) (dl-next lst)))
      (setf (dl-next lst) elt))
    elt))

(defun dl-list (&rest args)
    "Create a linked list from the arguments provided as input"
  (reduce #'dl-insert args
          :from-end t :initial-value nil))

(defun dl-remove (lst)
    "Remove the node from the linked list"
  (if (dl-prev lst)
      (setf (dl-next (dl-prev lst)) (dl-next lst)))
  (if (dl-next lst)
      (setf (dl-prev (dl-next lst)) (dl-prev lst)))
  (dl-next lst))

(defun dl-nextnode (lst)
    "Return the next node in the list
    Returns two values, 
        The next node in the list when dl-next is not nil
        A value indicating if the next node is returned, or nill if the last node in the list"
    (if (null (dl-next lst))
        (values lst nil)
        (values (dl-next lst) t)))

(defun dl-prevnode (lst)
    "Return the previous node in the list
    Returns two values, 
        The previous node in the list if dl-prev is not nil
        A value indicating if the previous node is returned, or nill if the first node in the list"    
    (if (null (dl-prev lst))
        (values lst nil)
        (values (dl-prev lst) t)))

(defun dl-find (dl func)
    "Find the first node in the list where the test function returns true
    Searches front to back, starting at dl, which may not necessarily be the 
    front of the list" 
    (let ((obj dl))
        (loop
            (when (null obj) (return nil))
            (if (funcall func obj)
                (return obj)
                (setf obj (dl-next obj))))))

;;;;; End: Link list functions
;;;;;


(defstruct (bitplane)
        zorder
        start
        end)

(let ((bitplanes nil))
    (defun bitplanes ()
        bitplanes)
    (defun first-bitplane ()
        (if (null bitplanes)
            nil
            (if (null (dl-prev bitplanes))
                bitplanes
                (setf bitplanes (dl-prev bitplanes)))))
    (defun set-bitplane (bitplane)
        (setf bitplanes (dl-list bitplane)))
    (defun remove-bitplanes ()
        (setf bitplanes nil)))


(defclass sprite ()
    (
        (id :accessor sprite-id :initform nil :initarg :id)
        (x :accessor sprite-x :initform 0 :initarg :x)
        (y :accessor sprite-y :initform 0 :initarg :y)
        (zorder :accessor sprite-zorder :initform 0 :initarg :zorder)))

(defun new-bitplane (zorder)
    (make-bitplane :zorder zorder))

(defun addto-bitplane (obj bitplane)
    (cond 
        ((null (bitplane-end bitplane))
            (setf (bitplane-end bitplane) (dl-list obj))
            (setf (bitplane-start bitplane) (bitplane-end bitplane)))
        (t
            (setf (bitplane-end bitplane) (dl-append obj (bitplane-end bitplane))))))

;Removes a node from the bitplane.
(defun remove-node-from-bitplane (obj bitplane)
    (when (null (dl-next obj))
        (setf (bitplane-end bitplane) (dl-prev obj)))
    (when (null (dl-prev obj))
        (setf (bitplane-start bitplane) (dl-next obj)))
    (dl-remove obj))

;Finds the node containing object, then calls remove-node-from-bitplane
(defun remove-from-bitplane (object bitplane)
    (let ((obj (dl-find (bitplane-start bitplane) 
        #'(lambda (node)
            (if (equal (dl-data node) object)
                node
                nil)))))
        (when obj
            (remove-node-from-bitplane obj bitplane))))

(defun get-zorder (obj)
    (cond 
        ((bitplane-p obj)
            (bitplane-zorder obj))
        ((dl-p obj)
            (bitplane-zorder (dl-data obj)))))

;Iterates through the list of bitplanes.
; Returns the bitplane, if bitplane == zorder.
; Returns the previous bitplane if bitplanes > zorder
; Returns 
(defun find-bitplane (zorder bitplanes)
    (let ((zorder (get-zorder bitplane)))   
        (cond 
            ((null bitplanes)
                nil)
            ((null (dl-next bitplanes))
                (dl-append bitplane bitplanes))
            ((> (get-zorder bitplanes) zorder)
                (dl-insert bitplane bitplanes))
            ((equal zorder (get-zorder bitplanes))
                nil)
            (t
                (add-bitplane bitplane (dl-nextnode bitplanes))))))

(defun add-bitplane (bitplane bitplanes)
    (let ((zorder (get-zorder bitplane)))   
        (cond 
            ((null bitplanes)
                (set-bitplane bitplane))
            ((null (dl-next bitplanes))
                (dl-append bitplane bitplanes))
            ((equal zorder (get-zorder bitplanes))
                nil)
            (t
                (add-bitplane bitplane (dl-nextnode bitplanes))))))

(defun add-sprite-to-bitplane (sprite bitplanes)
    (cond
        ((null bitplanes)
            (addto-bitplane sprite (add-bitplane (sprite-zorder sprite))))
        ((> (get-zorder bitplanes) (sprite-zorder sprite))
            (dl-insert bitplane bitplanes))
        ((null (dl-next bitplane))
            (dl-append bitplane bitplanes))
        (t
            (add-sprite-to-bitplane bitplanes (dl-nextnode bitplanes)))))

(defun add-sprite (sprite)
    (add-sprite-to-bitplane sprite (first-bitplane)))
           
                    
(defun return-bitplane (zorder bitplanes)
    (when (null bitplanes)
        (set-bitplane (dl-list (new-bitplane zorder)))
        (setf bitplanes (bitplanes)))
    (multiple-value-bind (bitplane pos) (find-bitplane zorder bitplanes)
        (cond
            ((equal pos 'c)
                (values (dl-data bitplane)))
            ((equal pos 'p)
                (setf bitplane (dl-insert (new-bitplane zorder) bitplane))
                (first-bitplane)
                (values (dl-data bitplane)))
            ((equal pos 'n)
                (values (dl-data (dl-append (new-bitplane zorder) bitplane)))))))


(defun add-object (spr)
    (addto-bitplane 
        (return-bitplane (sprite-zorder spr) (bitplanes))
        spr))
            
            











        




(defclass engine ()
    (
        (surface :accessor engine-surface #| get surface with DISPLAY-SURFACE|#)
        (sprites :accessor engine-sprites :initform (make-instance 'sprites))
        ))



            
            

(defun bouncing-ball ()
    (let (
            (width 800) (height 600)
            (video-flags (list                  
                    #|sdl:SDL_FULLSCREEN 
                    sdl:SDL_HWSURFACE
                    sdl:SDL_DOUBLEBUF|#
                    sdl:SDL_SWSURFACE))
            (pBitmap nil)
            (rcDst (sdl:new-rect))
            (dy 6) (dx 4)
            (max-right 0) (max-height 0))
       
        (sdl:with-sdl-init (sdl:SDL_INIT_VIDEO)
            (unless (sdl:set-videomode width height :flags video-flags)
                (fformat "FAILED: set-videomode, cannot set the video mode")
                (return))
            
            ; Initialize the engine
            (engine-init)
            ; Set the video surface
            (set-videosurface (get-engine) (display-surface))
            
            (unless (sdl:add-surface 'b-ball (sdl:sdl-DisplayFormat (sdl:sdl-loadbmp "b-ball.bmp")))
                (fformat "Cannot find b-ball.bmp")
                (return))
            
            (setf pBitmap (sdl:get-surface 'b-ball))
                                       
            (with-c-struct (var rcDst sdl:SDL_Rect)
                (setf
                    sdl::w (ct:cref sdl:SDL_Surface pBitmap sdl::w)
                    sdl::h (ct:cref sdl:SDL_Surface pBitmap sdl::h)
                    sdl::x 0
                    sdl::y 0))
        
            (with-c-struct (var rcDst sdl:sdl_rect)
                (setf 
                    max-height (- height sdl::h)
                    max-right (- width sdl::w)))
        
            (sdl:with-sdl-events 
                (:quit 
                    t)
                (:keydown (state keysym)
                    (when (eql (sdl:get-key keysym) sdl:SDLK_ESCAPE)
                        (sdl:push-quitevent)))
                (:idle
                    (sdl:SDL_FillRect (sdl:display-surface) NULL 0)
                
                    (sdl:SDL_BlitSurface pBitmap NULL (sdl:display-surface) rcDst)
                             
                    (with-c-struct (var rcdst sdl:sdl_rect)
                        (incf sdl::x dx)
                        (incf sdl::y dy)
                        (when (or (<= sdl::x 0) (>= sdl::x max-right))
                            (setf dx (- dx)))
                        (when (or (<= sdl::y 0) (>= sdl::y max-height))
                            (setf dy (- dy))))
                                               
                    (sdl:sdl-Flip))))
            
        (unless (sdl:sdl-init-success)
            (sdl:fformat "ERROR: sdl-init FAILED to initialize"))))
                    
;;; Change the current directory to the path where the file "b-ball.bmp" is located.
;;; e.g. use    (ccl:get-current-directory)
;;;             (ccl:set-current-directory "F:/Documents and Settings/Crook/My Documents/dev/lisp/corman-SDL/current")
;;; Run the example using...    
;;; (th:create-thread #'bouncing-ball)

;;; Build the exe using...
;;; (SAVE-APPLICATION "bouncing-ball.exe" 'bouncing-ball :static t)
