; Create a new level

(set-bitplane (new-bitplane 5))

(addto-bitplane (bitplanes) 'obj-1)
(addto-bitplane (bitplanes) 'obj-2)
(addto-bitplane (bitplanes) 'obj-5)
(addto-bitplane (bitplanes) 'obj-10)

(bitplanes)

(remove-from-bitplane (bitplanes) 'obj-10)

(add-bitplane (new-bitplane 5) (bitplanes))
(add-bitplane (new-bitplane 7) (bitplanes))
(add-bitplane (new-bitplane 7) (bitplanes))
(first-bitplane)
(bitplanes)